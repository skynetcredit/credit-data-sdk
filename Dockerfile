
FROM registry.cn-hangzhou.aliyuncs.com/phpfpm/phpfpm-end AS builder

COPY composer.json /var/www/html/
RUN composer install && composer dump-autoload --optimize
COPY . /var/www/html/

FROM registry.cn-hangzhou.aliyuncs.com/marmot/busybox
COPY --from=builder /var/www/html /var/www/html
RUN chown -R www-data:www-data /var/www/html

