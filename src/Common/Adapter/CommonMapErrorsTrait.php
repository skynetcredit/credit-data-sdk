<?php
namespace Qxy\CreditData\Common\Adapter;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
trait CommonMapErrorsTrait
{
    public function commonMapErrors()
    {
        return [
            10 => RESOURCE_NOT_EXIST
        ];
    }
}
