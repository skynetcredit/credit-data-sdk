<?php
namespace Qxy\CreditData\Common\Adapter;

use Qxy\CreditData\Common\Model\IOperateAble;

interface IOperateAbleAdapter
{
    public function add(IOperateAble $operateAbleObject) : bool;

    public function edit(IOperateAble $operateAbleObject) : bool;
}
