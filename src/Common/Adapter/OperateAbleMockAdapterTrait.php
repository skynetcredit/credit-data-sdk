<?php
namespace Qxy\CreditData\Common\Adapter;

use Qxy\CreditData\Common\Model\IOperateAble;

trait OperateAbleMockAdapterTrait
{
    public function add(IOperateAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);
        return true;
    }

    public function edit(IOperateAble $operatAbleObject) : bool
    {
        unset($operatAbleObject);
        return true;
    }
}
