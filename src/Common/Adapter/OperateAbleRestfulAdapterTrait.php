<?php
namespace Qxy\CreditData\Common\Adapter;

use Qxy\CreditData\Common\Model\IOperateAble;

trait OperateAbleRestfulAdapterTrait
{
    abstract protected function addAction(IOperateAble $enableAbleObject) : bool;
    abstract protected function editAction(IOperateAble $enableAbleObject) : bool;

    public function add(IOperateAble $enableAbleObject) : bool
    {
        return $this->addAction($enableAbleObject);
    }

    public function edit(IOperateAble $enableAbleObject) : bool
    {
        return $this->editAction($enableAbleObject);
    }
}
