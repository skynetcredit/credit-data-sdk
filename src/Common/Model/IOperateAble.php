<?php
namespace Qxy\CreditData\Common\Model;

interface IOperateAble
{
    public function add() : bool;
    
    public function edit() : bool;
}
