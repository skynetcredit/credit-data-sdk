<?php
namespace Qxy\CreditData\Common\Model;

use Qxy\CreditData\Common\Adapter\IOperateAbleAdapter;

trait OperateAbleTrait
{
    public function add() : bool
    {
        $repository = $this->getIOperateAbleAdapter();

        return $repository->add($this);
    }

    public function edit() : bool
    {
        $repository = $this->getIOperateAbleAdapter();

        return $repository->edit($this);
    }

    abstract protected function getIOperateAbleAdapter() : IOperateAbleAdapter;
}
