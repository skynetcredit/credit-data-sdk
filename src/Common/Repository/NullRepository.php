<?php
namespace Qxy\CreditData\Common\Repository;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullRepository implements INull
{
    private static $instance;
    
    private function __construct()
    {
    }

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    private function repositoryNotExist() : bool
    {
        Core::setLastError(TRANSLATOR_NOT_EXIST);
        return false;
    }

    public function getActualAdapter()
    {
        return $this->repositoryNotExist();
    }

    public function getMockAdapter()
    {
        return $this->repositoryNotExist();
    }
}
