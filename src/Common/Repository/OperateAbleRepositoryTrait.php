<?php
namespace Qxy\CreditData\Common\Repository;

use Qxy\CreditData\Common\Model\IOperateAble;

trait OperateAbleRepositoryTrait
{
    public function add(IOperateAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->add($operatAbleObject);
    }

    public function edit(IOperateAble $operatAbleObject) : bool
    {
        return $this->getAdapter()->edit($operatAbleObject);
    }
}
