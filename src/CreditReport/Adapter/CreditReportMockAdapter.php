<?php
namespace Qxy\CreditData\CreditReport\Adapter;

use Qxy\CreditData\CreditReport\Model\CreditReport;
use Qxy\CreditData\CreditReport\Utils\MockFactory;

/**
 * @codeCoverageIgnore
 */
class CreditReportMockAdapter implements ICreditReportAdapter
{
    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateDictionaryObject($id);
    }

    public function fetchListAsync(array $ids)
    {
        unset($ids);
        return array();
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }

    public function fetchOne($id)
    {
        return new CreditReport();
    }

    public function fetchList(array $ids): array
    {
        return array();
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ): array {
        return array();
    }
    
    public function fetchCreditReport($subject, $identify, $scoringModelId)
    {
        return array();
    }
}
