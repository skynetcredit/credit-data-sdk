<?php
namespace Qxy\CreditData\CreditReport\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IAsyncAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Qxy\CreditData\CreditReport\Model\NullCreditReport;
use Qxy\CreditData\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;
use Qxy\CreditData\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Qxy\CreditData\CreditReport\Translator\CreditReportRestfulTranslator;

class CreditReportRestfulAdapter extends GuzzleAdapter implements ICreditReportAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREDIT_REPORT_LIST'=>[
            'fields'=>[]
        ],
        'CREDIT_REPORT_FETCH_ONE'=>[
            'fields'=>[],
            'include'=> 'subject'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CreditReportRestfulTranslator();
        $this->resource = 'creditReports';
        $this->scenario = array();
    }
    
    protected function getResource(): string
    {
        return $this->resource;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCreditReport::getInstance());
    }

    public function fetchCreditReport($subject, $identify, $scoringModelId)
    {
        return $this->fetchCreditReportAction($subject, $identify, $scoringModelId);
    }

    protected function fetchCreditReportAction($subject, $identify, $scoringModelId)
    {
        $this->get(
            $this->getResource().'/'.$subject.'/'.$identify.'/'.$scoringModelId
        );
        
        return $this->isSuccess() ? $this->translateToObject() : NullCreditReport::getInstance();
    }

    public function fetchCreditReportAsync($subject, $identify, $scoringModelId)
    {
        return $this->getAsync(
            $this->getResource().'/'.$subject.'/'.$identify.'/'.$scoringModelId
        );
    }
}
