<?php
namespace Qxy\CreditData\CreditReport\Adapter;

use Qxy\CreditData\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface ICreditReportAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
    public function fetchCreditReport($subject, $identify, $scoringModelId);
}
