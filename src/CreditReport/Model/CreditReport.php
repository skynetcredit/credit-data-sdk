<?php
namespace Qxy\CreditData\CreditReport\Model;

use Marmot\Common\Model\IObject;
use Marmot\Core;
use Marmot\Common\Model\Object;
use Qxy\CreditData\Enterprise\Model\Enterprise;

class CreditReport implements IObject
{
    use Object;

    const RULES_TYPE = array(
        'MENU' => 1,
        'NUMBER' => 2,
    );

    private $id;
    /**
     * @var string $score 来源总分
    */
    private $score;
    /**
     * @var array $sourceRubric 指标内容
    */
    private $sourceRubric;
    /**
     * [$enterprise 关联企业]
     * @var [enterprise]
     */
    private $enterprise;
    
    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->score = 0;
        $this->sourceRubric = array();
        $this->enterprise = new Enterprise();

        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->score);
        unset($this->sourceRubric);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setScore(string $score) : void
    {
        $this->score = $score;
    }

    public function getScore() : string
    {
        return $this->score;
    }

    public function setSourceRubric(array $sourceRubric) : void
    {
        $this->sourceRubric = $sourceRubric;
    }

    public function getSourceRubric() : array
    {
        return $this->sourceRubric;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }
}
