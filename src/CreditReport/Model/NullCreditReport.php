<?php
namespace Qxy\CreditData\CreditReport\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullCreditReport extends CreditReport implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
