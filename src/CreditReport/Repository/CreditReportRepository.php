<?php
namespace Qxy\CreditData\CreditReport\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Marmot\Interfaces\IAsyncAdapter;
use Qxy\CreditData\Common\Repository\AsyncRepositoryTrait;
use Qxy\CreditData\Common\Repository\ErrorRepositoryTrait;
use Qxy\CreditData\Common\Repository\FetchRepositoryTrait;
use Qxy\CreditData\CreditReport\Adapter\CreditReportRestfulAdapter;
use Qxy\CreditData\CreditReport\Adapter\CreditReportMockAdapter;
use Qxy\CreditData\CreditReport\Adapter\ICreditReportAdapter;

class CreditReportRepository extends Repository implements ICreditReportAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREDIT_REPORT_LIST';
    const FETCH_ONE_MODEL_UN = 'CREDIT_REPORT_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CreditReportRestfulAdapter(
            Core::$container->has('creditData.url') ? Core::$container->get('creditData.url') : '', //phpcs:ignore
            Core::$container->has('creditData.authKey') ? Core::$container->get('creditData.authKey') : [] //phpcs:ignore
        );
    }

    public function getAdapter() : ICreditReportAdapter
    {
        return $this->adapter;
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICreditReportAdapter
    {
        return new CreditReportMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function fetchCreditReport($subject, $identify, $scoringModelId)
    {
        return $this->getAdapter()->fetchCreditReport($subject, $identify, $scoringModelId);
    }

    public function fetchCreditReportAsync($subject, $identify, $scoringModelId)
    {
        $adapter = $this->getAdapter();
        return $adapter instanceof IAsyncAdapter
            ? $adapter->fetchCreditReportAsync($subject, $identify, $scoringModelId)
            : '';
    }
}
