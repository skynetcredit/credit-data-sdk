<?php
namespace Qxy\CreditData\CreditReport\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Qxy\CreditData\Common\Translator\RestfulTranslatorTrait;
use Qxy\CreditData\CreditReport\Model\CreditReport;
use Qxy\CreditData\CreditReport\Model\NullCreditReport;

use Qxy\CreditData\Enterprise\Translator\EnterpriseRestfulTranslator;

class CreditReportRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getEnterpriseRestfulTranslator()
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $creditReport = null)
    {
        return $this->translateToObject($expression, $creditReport);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $creditReport = null)
    {
        if (empty($expression)) {
            return NullCreditReport::getInstance();
        }

        if ($creditReport == null) {
            $creditReport = new CreditReport();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $creditReport->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : [];

        if (isset($attributes['score'])) {
            $creditReport->setScore($attributes['score']);
        }
        if (isset($attributes['sourceRubric'])) {
            $creditReport->setSourceRubric($attributes['sourceRubric']);
        }
        if (isset($attributes['createTime'])) {
            $creditReport->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $creditReport->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $creditReport->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $creditReport->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['subject']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['subject']['data']);
            $creditReport->setEnterprise(
                $this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise)
            );
        }

        return $creditReport;
    }

     /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($creditReport, array $keys = array())
    {
        unset($creditReport);
        unset($keys);
        return array();
    }
}
