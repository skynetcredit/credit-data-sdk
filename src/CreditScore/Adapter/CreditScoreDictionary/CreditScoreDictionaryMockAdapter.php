<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary;

use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\CreditScoreDictionary;
use Qxy\CreditData\CreditScore\Utils\CreditScoreDictionary\MockFactory;

/**
 * @codeCoverageIgnore
 */
class CreditScoreDictionaryMockAdapter implements ICreditScoreDictionaryAdapter
{
    public function fetchOneAsync(int $id)
    {
        unset($id);
        return MockFactory::generateDictionaryObject($id);
    }

    public function fetchListAsync(array $ids)
    {
        unset($ids);
        return array();
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }

    public function fetchOne($id)
    {
        unset($id);
        return new CreditScoreDictionary();
    }

    public function fetchList(array $ids): array
    {
        unset($ids);
        return array();
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }
}
