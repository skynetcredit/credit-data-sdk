<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\NullCreditScoreDictionary;
use Qxy\CreditData\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;
use Qxy\CreditData\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Qxy\CreditData\CreditScore\Translator\CreditScoreDictionary\CreditScoreDictionaryRestfulTranslator;

class CreditScoreDictionaryRestfulAdapter extends GuzzleAdapter implements ICreditScoreDictionaryAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREDIT_SCORE_DICTIONARY_LIST'=>[
            'fields'=>[]
        ],
        'CREDIT_SCORE_DICTIONARY_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CreditScoreDictionaryRestfulTranslator();
        $this->resource = 'dictionaries';
        $this->scenario = array();
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCreditScoreDictionary::getInstance());
    }
}
