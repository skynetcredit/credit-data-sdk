<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary;

use Qxy\CreditData\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface ICreditScoreDictionaryAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
