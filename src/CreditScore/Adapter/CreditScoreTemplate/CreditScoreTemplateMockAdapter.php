<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate;

use Qxy\CreditData\Common\Adapter\OperateAbleMockAdapterTrait;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;

/**
 * @codeCoverageIgnore
 */
class CreditScoreTemplateMockAdapter implements ICreditScoreTemplateAdapter
{
    use OperateAbleMockAdapterTrait;

    public function fetchOneAsync(int $id)
    {
        unset($id);
        return new CreditScoreTemplate();
    }

    public function fetchListAsync(array $ids)
    {
        unset($ids);
        return array();
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }

    public function fetchOne($id)
    {
        unset($id);
        return new CreditScoreTemplate();
    }

    public function fetchList(array $ids): array
    {
        unset($ids);
        return array();
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ): array {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }
}
