<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Qxy\CreditData\Common\Adapter\OperateAbleRestfulAdapterTrait;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\NullCreditScoreTemplate;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;
use Qxy\CreditData\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;
use Qxy\CreditData\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Qxy\CreditData\CreditScore\Translator\CreditScoreTemplate\CreditScoreTemplateRestfulTranslator;

class CreditScoreTemplateRestfulAdapter extends GuzzleAdapter implements ICreditScoreTemplateAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'CREDIT_SCORE_TEMPLATE_LIST'=>[
            'fields'=>[],
        ],
        'CREDIT_SCORE_TEMPLATE_FETCH_ONE'=>[
            'fields'=>[],
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CreditScoreTemplateRestfulTranslator();
        $this->resource = 'scoringModels';
        $this->scenario = array();
    }

    protected function getResource(): string
    {
        return $this->resource;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullCreditScoreTemplate::getInstance());
    }

    protected function addAction(CreditScoreTemplate $creditScoreTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreTemplate,
            array(
                'name',
                'applicationObject',
                'dataDomain',
                'dataSourceAndIndex'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );
        if ($this->isSuccess()) {
            $this->translateToObject($creditScoreTemplate);
            return true;
        }

        return false;
    }

    protected function editAction(CreditScoreTemplate $creditScoreTemplate) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $creditScoreTemplate,
            array(
                'name',
                'applicationObject',
                'dataDomain',
                'dataSourceAndIndex'
            )
        );
        $this->patch(
            $this->getResource().'/'.$creditScoreTemplate->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($creditScoreTemplate);
            return true;
        }
   
        return false;
    }
}
