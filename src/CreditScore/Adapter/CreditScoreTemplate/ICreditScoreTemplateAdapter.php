<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate;

use Qxy\CreditData\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;
use Qxy\CreditData\Common\Adapter\IOperateAbleAdapter;

interface ICreditScoreTemplateAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperateAbleAdapter
{
}
