<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreDictionary;

use Marmot\Common\Model\IObject;
use Marmot\Core;
use Marmot\Common\Model\Object;

use Qxy\CreditData\CreditScore\Repository\CreditScoreDictionary\CreditScoreDictionaryRepository;

class CreditScoreDictionary implements IObject
{
    use Object;

    const CREDIT_SCORE_DICTIONARY_STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2,
        'DELETE' => -4
    );

    const CREDIT_SCORE_DICTIONARY_CATEGORY = array(
        'NULL' => 0,
        'APPLICATION_OBJECT' => 1,
        'APPLICATION_DOMAIN' => 2,
        'DATA_SOURCE' => 3,
    );

    private $id;

    private $name;

    private $parentId;

    private $category;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id   = !empty($id) ? $id : 0;
        $this->name = '';
        $this->parentId = 0;
        $this->category = self::CREDIT_SCORE_DICTIONARY_CATEGORY['NULL'];
        $this->status = self::CREDIT_SCORE_DICTIONARY_STATUS['ENABLED'];
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new CreditScoreDictionaryRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->parentId);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setParentId(int $parentId) : void
    {
        $this->parentId = $parentId;
    }

    public function getParentId() : int
    {
        return $this->parentId;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    public function getRepository() : CreditScoreDictionaryRepository
    {
        return $this->repository;
    }
}
