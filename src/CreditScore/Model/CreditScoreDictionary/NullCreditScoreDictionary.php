<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreDictionary;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullCreditScoreDictionary extends CreditScoreDictionary implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
