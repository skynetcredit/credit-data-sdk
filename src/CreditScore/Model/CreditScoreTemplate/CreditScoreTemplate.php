<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreTemplate;

use Marmot\Common\Model\IObject;
use Marmot\Core;

use Marmot\Common\Model\Object;
use Qxy\CreditData\Common\Adapter\IOperateAbleAdapter;
use Qxy\CreditData\Common\Model\IOperateAble;
use Qxy\CreditData\Common\Model\OperateAbleTrait;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter;
use Qxy\CreditData\CreditScore\Repository\CreditScoreTemplate\CreditScoreTemplateRepository;

class CreditScoreTemplate implements IOperateAble, IObject
{
    use OperateAbleTrait, Object;

    private $id;

    private $name;

    private $applicationObject;

    private $dataDomain;

    private $dataSourceAndIndex;

    private $repository;

    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->applicationObject = 0;
        $this->dataDomain = array();
        $this->dataSourceAndIndex = array();
        $this->status = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->statusTime = 0;
        $this->repository = new CreditScoreTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->name);
        unset($this->applicationObject);
        unset($this->dataDomain);
        unset($this->dataSourceAndIndex);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }
    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }
    public function getName() : string
    {
        return $this->name;
    }

    public function setApplicationObject(int $applicationObject) : void
    {
        $this->applicationObject = $applicationObject;
    }
    public function getApplicationObject() : int
    {
        return $this->applicationObject;
    }

    public function setDataDomain(array $dataDomain) : void
    {
        $this->dataDomain = $dataDomain;
    }
    public function getDataDomain() : array
    {
        return $this->dataDomain;
    }

    public function setDataSourceAndIndex(array $dataSourceAndIndex) : void
    {
        $this->dataSourceAndIndex = $dataSourceAndIndex;
    }
    public function getDataSourceAndIndex() : array
    {
        return $this->dataSourceAndIndex;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
    public function getStatus() : int
    {
        return $this->status;
    }

    protected function getRepository() : ICreditScoreTemplateAdapter
    {
        return $this->repository;
    }

    protected function getIOperateAbleAdapter() : IOperateAbleAdapter
    {
        return $this->getRepository();
    }
}
