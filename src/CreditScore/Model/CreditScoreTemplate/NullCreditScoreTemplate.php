<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreTemplate;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Qxy\CreditData\Common\Model\NullOperateAbleTrait;
use Qxy\CreditData\Common\Model\NullEnableAbleTrait;

class NullCreditScoreTemplate extends CreditScoreTemplate implements INull
{
    use NullOperateAbleTrait;

    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
