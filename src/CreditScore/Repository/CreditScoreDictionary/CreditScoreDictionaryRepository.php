<?php
namespace Qxy\CreditData\CreditScore\Repository\CreditScoreDictionary;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Qxy\CreditData\Common\Repository\AsyncRepositoryTrait;
use Qxy\CreditData\Common\Repository\ErrorRepositoryTrait;
use Qxy\CreditData\Common\Repository\FetchRepositoryTrait;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\CreditScoreDictionaryRestfulAdapter;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\CreditScoreDictionaryMockAdapter;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\ICreditScoreDictionaryAdapter;

class CreditScoreDictionaryRepository extends Repository implements ICreditScoreDictionaryAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREDIT_SCORE_DICTIONARY_LIST';
    const FETCH_ONE_MODEL_UN = 'CREDIT_SCORE_DICTIONARY_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CreditScoreDictionaryRestfulAdapter(
            Core::$container->has('creditData.url') ? Core::$container->get('creditData.url') : '', //phpcs:ignore
            Core::$container->has('creditData.authKey') ? Core::$container->get('creditData.authKey') : [] //phpcs:ignore
        );
    }

    public function getAdapter() : ICreditScoreDictionaryAdapter
    {
        return $this->adapter;
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICreditScoreDictionaryAdapter
    {
        return new CreditScoreDictionaryMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
