<?php
namespace Qxy\CreditData\CreditScore\Repository\CreditScoreTemplate;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Qxy\CreditData\Common\Repository\AsyncRepositoryTrait;
use Qxy\CreditData\Common\Repository\ErrorRepositoryTrait;
use Qxy\CreditData\Common\Repository\FetchRepositoryTrait;
use Qxy\CreditData\Common\Repository\OperateAbleRepositoryTrait;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateRestfulAdapter;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateMockAdapter;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter;

class CreditScoreTemplateRepository extends Repository implements ICreditScoreTemplateAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperateAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'CREDIT_SCORE_TEMPLATE_LIST';
    const FETCH_ONE_MODEL_UN = 'CREDIT_SCORE_TEMPLATE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CreditScoreTemplateRestfulAdapter(
            Core::$container->has('creditData.url') ? Core::$container->get('creditData.url') : '',
            Core::$container->has('creditData.authKey') ? Core::$container->get('creditData.authKey') : []
        );
    }

    protected function getActualAdapter() : ICreditScoreTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ICreditScoreTemplateAdapter
    {
        return new CreditScoreTemplateMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
