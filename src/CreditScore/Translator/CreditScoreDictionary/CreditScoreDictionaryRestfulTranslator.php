<?php
namespace Qxy\CreditData\CreditScore\Translator\CreditScoreDictionary;

use Marmot\Interfaces\IRestfulTranslator;
use Qxy\CreditData\Common\Translator\RestfulTranslatorTrait;
use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\CreditScoreDictionary;
use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\NullCreditScoreDictionary;

class CreditScoreDictionaryRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $creditScoreDictionary = null)
    {
        return $this->translateToObject($expression, $creditScoreDictionary);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $creditScoreDictionary = null)
    {
        if (empty($expression)) {
            return NullCreditScoreDictionary::getInstance();
        }

        if ($creditScoreDictionary == null) {
            $creditScoreDictionary = new CreditScoreDictionary();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $creditScoreDictionary->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : [];

        if (isset($attributes['name'])) {
            $creditScoreDictionary->setName($attributes['name']);
        }
        if (isset($attributes['parentId'])) {
            $creditScoreDictionary->setParentId($attributes['parentId']);
        }
        if (isset($attributes['category'])) {
            $creditScoreDictionary->setCategory($attributes['category']);
        }
        if (isset($attributes['createTime'])) {
            $creditScoreDictionary->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $creditScoreDictionary->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $creditScoreDictionary->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $creditScoreDictionary->setStatusTime($attributes['statusTime']);
        }

        return $creditScoreDictionary;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($creditScoreDictionary, array $keys = array())
    {
        $expression = array();

        if (!$creditScoreDictionary instanceof CreditScoreDictionary) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'parentId',
                'status'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'dictionaries'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $creditScoreDictionary->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $creditScoreDictionary->getName();
        }
        if (in_array('parentId', $keys)) {
            $attributes['parentId'] = $creditScoreDictionary->getParentId();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $creditScoreDictionary->getStatus();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
