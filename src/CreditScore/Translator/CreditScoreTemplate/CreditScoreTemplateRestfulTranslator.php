<?php
namespace Qxy\CreditData\CreditScore\Translator\CreditScoreTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use Qxy\CreditData\Common\Translator\RestfulTranslatorTrait;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\NullCreditScoreTemplate;

class CreditScoreTemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $creditScoreTemplate = null)
    {
        return $this->translateToObject($expression, $creditScoreTemplate);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $creditScoreTemplate = null)
    {
        if (empty($expression)) {
            return NullCreditScoreTemplate::getInstance();
        }

        if ($creditScoreTemplate == null) {
            $creditScoreTemplate = new CreditScoreTemplate();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $creditScoreTemplate->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $creditScoreTemplate->setName($attributes['name']);
        }
        if (isset($attributes['applicationObject'])) {
            $creditScoreTemplate->setApplicationObject($attributes['applicationObject']);
        }
        if (isset($attributes['dataDomain'])) {
            $creditScoreTemplate->setDataDomain($attributes['dataDomain']);
        }
        if (isset($attributes['dataSourceAndIndex'])) {
            $creditScoreTemplate->setDataSourceAndIndex($attributes['dataSourceAndIndex']);
        }
        if (isset($attributes['createTime'])) {
            $creditScoreTemplate->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $creditScoreTemplate->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $creditScoreTemplate->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $creditScoreTemplate->setStatusTime($attributes['statusTime']);
        }

        return $creditScoreTemplate;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($creditScoreTemplate, array $keys = array())
    {
        $expression = array();

        if (!$creditScoreTemplate instanceof CreditScoreTemplate) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'applicationObject',
                'dataDomain',
                'dataSourceAndIndex'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'scoringModels'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $creditScoreTemplate->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $creditScoreTemplate->getName();
        }
        if (in_array('applicationObject', $keys)) {
            $attributes['applicationObject'] = $creditScoreTemplate->getApplicationObject();
        }
        if (in_array('dataDomain', $keys)) {
            $attributes['dataDomain'] = $creditScoreTemplate->getDataDomain();
        }
        if (in_array('dataSourceAndIndex', $keys)) {
            $attributes['dataSourceAndIndex'] = $creditScoreTemplate->getDataSourceAndIndex();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
