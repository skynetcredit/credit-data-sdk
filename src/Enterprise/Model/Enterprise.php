<?php
namespace Qxy\CreditData\Enterprise\Model;

use Marmot\Common\Model\IObject;
use Marmot\Core;
use Marmot\Common\Model\Object;

class Enterprise implements IObject
{
    use Object;

    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $name 主体名称(企业名称/姓名)
     */
    protected $name;
    /**
     * @var string $identify 主体代码(统一社会信用代码/身份证号)
     */
    protected $identify;
     /**
     * @var string $enterpriseType 企业类型
     */
    private $enterpriseType;
    /**
     * @var string $principal 法人信息
     */
    private $principal;
    /**
     * @var string $businessStatus 经营状态
     */
    private $businessStatus;
    /**
     * @var string $registrationAuthority 登记机关
     */
    private $registrationAuthority;
    /**
     * @var string $establishedDate 成立日期
     */
    private $establishedDate;
    
    public function __construct($id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->identify = '';
        $this->enterpriseType = '';
        $this->principal = '';
        $this->businessStatus = '';
        $this->registrationAuthority = '';
        $this->establishedDate = '0000-00-00';

        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->enterpriseType);
        unset($this->principal);
        unset($this->businessStatus);
        unset($this->registrationAuthority);
        unset($this->establishedDate);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setEnterpriseType(string $enterpriseType) : void
    {
        $this->enterpriseType = $enterpriseType;
    }

    public function getEnterpriseType() : string
    {
        return $this->enterpriseType;
    }

    public function setPrincipal(string $principal) : void
    {
        $this->principal = $principal;
    }

    public function getPrincipal() : string
    {
        return $this->principal;
    }

    public function setBusinessStatus(string $businessStatus) : void
    {
        $this->businessStatus = $businessStatus;
    }

    public function getBusinessStatus() : string
    {
        return $this->businessStatus;
    }

    public function setRegistrationAuthority(string $registrationAuthority) : void
    {
        $this->registrationAuthority = $registrationAuthority;
    }

    public function getRegistrationAuthority() : string
    {
        return $this->registrationAuthority;
    }

    public function setEstablishedDate(string $establishedDate) : void
    {
        $this->establishedDate = $establishedDate;
    }

    public function getEstablishedDate() : string
    {
        return $this->establishedDate;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
