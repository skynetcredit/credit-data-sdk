<?php
namespace Qxy\CreditData\Enterprise\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullEnterprise extends Enterprise implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
