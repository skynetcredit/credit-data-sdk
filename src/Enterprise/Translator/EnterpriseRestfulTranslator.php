<?php
namespace Qxy\CreditData\Enterprise\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Qxy\CreditData\Common\Translator\RestfulTranslatorTrait;
use Qxy\CreditData\Enterprise\Model\Enterprise;
use Qxy\CreditData\Enterprise\Model\NullEnterprise;

class EnterpriseRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $enterprise = null)
    {
        return $this->translateToObject($expression, $enterprise);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $enterprise = null)
    {
        if (empty($expression)) {
            return NullEnterprise::getInstance();
        }

        if ($enterprise == null) {
            $enterprise = new Enterprise();
        }
        
        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $enterprise->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : [];

        if (isset($attributes['name'])) {
            $enterprise->setName($attributes['name']);
        }
        if (isset($attributes['unifiedSocialCreditCode'])) {
            $enterprise->setIdentify($attributes['unifiedSocialCreditCode']);
        }
        if (isset($attributes['enterpriseType'])) {
            $enterprise->setEnterpriseType($attributes['enterpriseType']);
        }
        if (isset($attributes['principal'])) {
            $enterprise->setPrincipal($attributes['principal']);
        }
        if (isset($attributes['businessStatus'])) {
            $enterprise->setBusinessStatus($attributes['businessStatus']);
        }
        if (isset($attributes['registrationAuthority'])) {
            $enterprise->setRegistrationAuthority($attributes['registrationAuthority']);
        }
        if (isset($attributes['establishedDate'])) {
            $enterprise->setEstablishedDate($attributes['establishedDate']);
        }
        if (isset($attributes['createTime'])) {
            $enterprise->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $enterprise->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $enterprise->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $enterprise->setStatusTime($attributes['statusTime']);
        }

        return $enterprise;
    }

     /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($enterprise, array $keys = array())
    {
        unset($enterprise);
        unset($keys);
        return array();
    }
}
