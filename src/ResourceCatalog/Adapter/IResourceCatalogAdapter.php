<?php
namespace Qxy\CreditData\ResourceCatalog\Adapter;

use Qxy\CreditData\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IResourceCatalogAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
