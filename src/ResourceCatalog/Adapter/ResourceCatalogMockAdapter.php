<?php
namespace Qxy\CreditData\ResourceCatalog\Adapter;

use Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog;
use Qxy\CreditData\ResourceCatalog\Utils\MockFactory;

/**
 * @codeCoverageIgnore
 */
class ResourceCatalogMockAdapter implements IResourceCatalogAdapter
{
    public function fetchOneAsync(int $id)
    {
        return MockFactory::generateDictionaryObject($id);
    }

    public function fetchListAsync(array $ids)
    {
        unset($ids);
        return array();
    }

    public function searchAsync(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) {
        unset($filter);
        unset($sort);
        unset($number);
        unset($size);
        return array();
    }

    public function fetchOne($id)
    {
        return new ResourceCatalog();
    }

    public function fetchList(array $ids): array
    {
        return array();
    }

    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ): array {
        return array();
    }
}
