<?php
namespace Qxy\CreditData\ResourceCatalog\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Qxy\CreditData\ResourceCatalog\Model\NullResourceCatalog;
use Qxy\CreditData\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;
use Qxy\CreditData\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Qxy\CreditData\ResourceCatalog\Translator\ResourceCatalogRestfulTranslator;

class ResourceCatalogRestfulAdapter extends GuzzleAdapter implements IResourceCatalogAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'RESOURCE_CATALOG_LIST'=>[
            'fields'=>[]
        ],
        'RESOURCE_CATALOG_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ResourceCatalogRestfulTranslator();
        $this->resource = 'resourceCatalogs';
        $this->scenario = array();
    }
    
    protected function getResource(): string
    {
        return $this->resource;
    }

    protected function getTranslator(): IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getMapErrors() : array
    {
        $commonMapErrors = $this->commonMapErrors();

        return $commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullResourceCatalog::getInstance());
    }
}
