<?php
namespace Qxy\CreditData\ResourceCatalog\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullResourceCatalog extends ResourceCatalog implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
