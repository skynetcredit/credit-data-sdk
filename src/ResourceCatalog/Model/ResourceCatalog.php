<?php
namespace Qxy\CreditData\ResourceCatalog\Model;

use Marmot\Common\Model\IObject;
use Marmot\Core;
use Marmot\Common\Model\Object;

class ResourceCatalog implements IObject
{
    use Object;

    const RULES_TYPE = array(
        'MENU' => 1,
        'NUMBER' => 2,
    );

    private $id;
    /**
     * @var string $name 资源目录名称
    */
    private $name;
    /**
     * @var int $dataDomain 应用对象
    */
    private $applicationObject;
    /**
     * @var int $dataDomain 应用领域
    */
    private $dataDomain;
    /**
     * @var int $dataSource 数据来源
    */
    private $dataSource;
    /**
     * @var int $rule 评分规则
    */
    private $rule;
    /**
     * @var array $enumOptions 枚举选项
    */
    private $enumOptions;
    /**
     * @var array $template 资源目录模版
    */
    private $template;

    public function __construct($id = '')
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->applicationObject = 0;
        $this->dataDomain = 0;
        $this->dataSource = 0;
        $this->rule = 0;
        $this->enumOptions = array();
        $this->template = array();

        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->applicationObject);
        unset($this->dataDomain);
        unset($this->dataSource);
        unset($this->rule);
        unset($this->enumOptions);
        unset($this->template);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setApplicationObject(int $applicationObject) : void
    {
        $this->applicationObject = $applicationObject;
    }

    public function getApplicationObject() : int
    {
        return $this->applicationObject;
    }

    public function setDataDomain(int $dataDomain) : void
    {
        $this->dataDomain = $dataDomain;
    }

    public function getDataDomain() : int
    {
        return $this->dataDomain;
    }

    public function setDataSource(int $dataSource) : void
    {
        $this->dataSource = $dataSource;
    }

    public function getDataSource() : int
    {
        return $this->dataSource;
    }

    public function setRule(int $rule) : void
    {
        $this->rule = $rule;
    }

    public function getRule() : int
    {
        return $this->rule;
    }

    public function setEnumOptions(array $enumOptions) : void
    {
        $this->enumOptions = $enumOptions;
    }

    public function getEnumOptions() : array
    {
        return $this->enumOptions;
    }

    public function setTemplate(array $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : array
    {
        return $this->template;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
