<?php
namespace Qxy\CreditData\ResourceCatalog\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use Qxy\CreditData\Common\Repository\AsyncRepositoryTrait;
use Qxy\CreditData\Common\Repository\ErrorRepositoryTrait;
use Qxy\CreditData\Common\Repository\FetchRepositoryTrait;
use Qxy\CreditData\ResourceCatalog\Adapter\ResourceCatalogRestfulAdapter;
use Qxy\CreditData\ResourceCatalog\Adapter\ResourceCatalogMockAdapter;
use Qxy\CreditData\ResourceCatalog\Adapter\IResourceCatalogAdapter;

class ResourceCatalogRepository extends Repository implements IResourceCatalogAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'RESOURCE_CATALOG_LIST';
    const FETCH_ONE_MODEL_UN = 'RESOURCE_CATALOG_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ResourceCatalogRestfulAdapter(
            Core::$container->has('creditData.url') ? Core::$container->get('creditData.url') : '', //phpcs:ignore
            Core::$container->has('creditData.authKey') ? Core::$container->get('creditData.authKey') : [] //phpcs:ignore
        );
    }

    public function getAdapter() : IResourceCatalogAdapter
    {
        return $this->adapter;
    }

    protected function getActualAdapter()
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IResourceCatalogAdapter
    {
        return new ResourceCatalogMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
