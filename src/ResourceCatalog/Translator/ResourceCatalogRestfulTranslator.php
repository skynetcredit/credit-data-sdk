<?php
namespace Qxy\CreditData\ResourceCatalog\Translator;

use Marmot\Interfaces\IRestfulTranslator;
use Qxy\CreditData\Common\Translator\RestfulTranslatorTrait;
use Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog;
use Qxy\CreditData\ResourceCatalog\Model\NullResourceCatalog;

class ResourceCatalogRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $resourceCatalog = null)
    {
        return $this->translateToObject($expression, $resourceCatalog);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $resourceCatalog = null)
    {
        if (empty($expression)) {
            return NullResourceCatalog::getInstance();
        }

        if ($resourceCatalog == null) {
            $resourceCatalog = new ResourceCatalog();
        }

        $data =  $expression['data'];

        if (isset($data['id'])) {
            $id = $data['id'];
            $resourceCatalog->setId($id);
        }

        $attributes = isset($data['attributes']) ? $data['attributes'] : [];

        if (isset($attributes['name'])) {
            $resourceCatalog->setName($attributes['name']);
        }
        if (isset($attributes['applicationObject'])) {
            $resourceCatalog->setApplicationObject($attributes['applicationObject']);
        }
        if (isset($attributes['dataDomain'])) {
            $resourceCatalog->setDataDomain($attributes['dataDomain']);
        }
        if (isset($attributes['dataSource'])) {
            $resourceCatalog->setDataSource($attributes['dataSource']);
        }
        if (isset($attributes['rule'])) {
            $resourceCatalog->setRule($attributes['rule']);
        }
        if (isset($attributes['enumOptions'])) {
            $resourceCatalog->setEnumOptions($attributes['enumOptions']);
        }
        if (isset($attributes['template'])) {
            $resourceCatalog->setTemplate($attributes['template']);
        }
        if (isset($attributes['createTime'])) {
            $resourceCatalog->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $resourceCatalog->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $resourceCatalog->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $resourceCatalog->setStatusTime($attributes['statusTime']);
        }

        return $resourceCatalog;
    }

     /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($resourceCatalog, array $keys = array())
    {
        unset($resourceCatalog);
        unset($keys);
        return array();
    }
}
