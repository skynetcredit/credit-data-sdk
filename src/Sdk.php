<?php
namespace Qxy\CreditData;

use Marmot\Interfaces\Application\ISdk;

define('CREDIT_DATA_SDK_ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);

class Sdk implements ISdk
{
    public function initErrorConfig() : void
    {
        include_once CREDIT_DATA_SDK_ROOT.'errorConfig.php';
    }

    public function getErrorDescriptions() : array
    {
        return include_once CREDIT_DATA_SDK_ROOT.'./errorDescriptionConfig.php';
    }

    public function initConfig() : void
    {
        include_once CREDIT_DATA_SDK_ROOT.'config.php';
    }
}
