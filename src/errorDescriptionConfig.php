<?php

return [
    CREDIT_SCORE_TEMPLATE_NAME_ERROR=>
       [
            'id'=>CREDIT_SCORE_TEMPLATE_NAME_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'CREDIT_SCORE_TEMPLATE_NAME_ERROR',
            'title'=>'名称格式错误，应为1-10位汉字',
            'detail'=>'名称格式错误，应为1-10位汉字',
            'source'=>[],
            'meta'=>[]
        ],
    CREDIT_SCORE_TEMPLATE_APPLICATION_OBJ_ERROR=>
       [
            'id'=>CREDIT_SCORE_TEMPLATE_APPLICATION_OBJ_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'CREDIT_SCORE_TEMPLATE_APPLICATION_OBJ_ERROR',
            'title'=>'应用对象格式错误',
            'detail'=>'应用对象格式错误',
            'source'=>[],
            'meta'=>[]
        ],
];
