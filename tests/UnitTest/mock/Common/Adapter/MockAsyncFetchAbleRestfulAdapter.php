<?php
namespace Qxy\CreditData\Common\Adapter;

class MockAsyncFetchAbleRestfulAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait;

    protected function getResource()
    {
        return 'test';
    }
}
