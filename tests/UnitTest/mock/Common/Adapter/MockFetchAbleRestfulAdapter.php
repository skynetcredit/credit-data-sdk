<?php
namespace Qxy\CreditData\Common\Adapter;

class MockFetchAbleRestfulAdapter
{
    use FetchAbleRestfulAdapterTrait;

    protected function getResource()
    {
        return 'test';
    }
}
