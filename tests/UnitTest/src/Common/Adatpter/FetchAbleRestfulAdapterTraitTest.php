<?php
namespace Qxy\CreditData\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Qxy\CreditData\Common\Model\MockObject;
use Qxy\CreditData\Common\Model\NullMockObject;

class FetchAbleRestfulAdapterTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
            ->setMethods(
                [
                    'get',
                    'translateToObjects',
                    'translateToObject',
                    'isSuccess'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testFetchOneSuccess()
    {
        $id = 1;
        $array = [];

        $mock = new MockObject();
        $nullMock = new NullMockObject();

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->willReturn('test/'.$id)
            ->willReturn($array);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->willReturn($mock);

        $result = $this->stub->fetchOne($id, $nullMock);
        $this->assertEquals($mock, $result);
    }

    public function testFetchOneFailure()
    {
        $id = 1;
        $array = [];

        $nullMock = new NullMockObject();

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->willReturn('test/'.$id)
            ->willReturn($array);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);

        $result = $this->stub->fetchOne($id, $nullMock);
        $this->assertEquals($nullMock, $result);
    }

    public function testFetchListSuccess()
    {
        $ids = array(1,2,3);
        $test = array();

        $testArray = array();

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with('test/'.implode(',', $ids))
            ->willReturn($testArray);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObjects')
            ->willReturn($test);

        $result = $this->stub->fetchList($ids);
        $this->assertEquals($test, $result);
    }

    public function testFetchListActionFailure()
    {
        $ids = array(1,2,3);

        $testArray = array();

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with('test/'.implode(',', $ids))
            ->willReturn($testArray);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);

        $this->stub->expects($this->exactly(0))
            ->method('translateToObjects');

        $result = $this->stub->fetchList($ids);
        $this->assertEquals(array(0, array()), $result);
    }

    public function testSearchSuccess()
    {
        $filter = array();
        $sort = array();
        $page = 0;
        $size = 10;
        
        $test = array();

        $testArray = array();

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with('test')
            ->willReturn($testArray);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('translateToObjects')
            ->willReturn($test);

        $result = $this->stub->search($filter, $sort, $page, $size);
        $this->assertEquals($test, $result);
    }

    public function testSearchActionFailure()
    {
        $filter = array();
        $sort = array();
        $page = 0;
        $size = 10;

        $testArray = array();

        $this->stub->expects($this->exactly(1))
            ->method('get')
            ->with('test')
            ->willReturn($testArray);

        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);

        $this->stub->expects($this->exactly(0))
            ->method('translateToObjects');

        $result = $this->stub->search($filter, $sort, $page, $size);
        $this->assertEquals(array(0,array()), $result);
    }
}
