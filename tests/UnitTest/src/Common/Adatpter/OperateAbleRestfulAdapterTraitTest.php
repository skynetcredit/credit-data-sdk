<?php
namespace Qxy\CreditData\Common\Adapter;

use PHPUnit\Framework\TestCase;

use Qxy\CreditData\Common\Model\MockObject;

class OperateAbleRestfulAdapterTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockForTrait(OperateAbleRestfulAdapterTrait::class);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testAdd()
    {
        $test = new MockObject();

        $this->stub->expects($this->any())
             ->method('addAction')
             ->with($test)
             ->will($this->returnValue(true));

        $this->assertTrue($this->stub->add($test));
    }

    public function testEdit()
    {
        $test = new MockObject();

        $this->stub->expects($this->any())
             ->method('editAction')
             ->with($test)
             ->will($this->returnValue(true));

        $this->assertTrue($this->stub->edit($test));
    }
}
