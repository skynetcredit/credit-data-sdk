<?php
namespace Qxy\CreditData\Common\Adapter;

class TestAsyncFetchAbleRestfulAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait;

    protected function getResource()
    {
        return 'test';
    }
}
