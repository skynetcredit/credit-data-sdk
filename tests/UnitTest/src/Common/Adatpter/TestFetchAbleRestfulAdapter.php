<?php
namespace Qxy\CreditData\Common\Adapter;

class TestFetchAbleRestfulAdapter
{
    use FetchAbleRestfulAdapterTrait;

    protected function getResource()
    {
        return 'test';
    }
}
