<?php
namespace Qxy\CreditData\Common\Model;

use Marmot\Core;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Qxy\CreditData\Common\Model\IOperateAble;
use Qxy\CreditData\Common\Model\OperateAbleTrait;

class MockObject implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    /**
     * @var int $id
     */
    protected $id;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getIOperateAbleAdapter()
    {
    }

    public function addAction()
    {
    }

    public function editAction()
    {
    }
}
