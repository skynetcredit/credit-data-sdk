<?php
namespace Qxy\CreditData\Common\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullOperateAbleTraitTest extends TestCase
{
    private $contract;

    public function setUp()
    {
        $this->contract = $this->getMockBuilder(TestOperateAble::class)
                    ->setMethods()->getMock();
    }

    public function tearDown()
    {
        unset($this->contract);
    }

    public function testAdd()
    {
        $result = $this->contract->add();
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->contract->edit();
        $this->assertFalse($result);
    }
}
