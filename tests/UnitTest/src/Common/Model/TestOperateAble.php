<?php
namespace Qxy\CreditData\Common\Model;

class TestOperateAble
{
    use NullOperateAbleTrait;

    public function resourceNotExist() : bool
    {
        return false;
    }
}
