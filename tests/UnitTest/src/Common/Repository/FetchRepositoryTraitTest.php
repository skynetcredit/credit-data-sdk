<?php
namespace Qxy\CreditData\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Qxy\CreditData\Common\Model\MockObject;

use Qxy\CreditData\Common\Adapter\IFetchAbleAdapter;

class FetchRepositoryTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TestFetchRepository::class)
                    ->setMethods(['getAdapter'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testFetchOne()
    {
        $id = 1;
        $mock = new MockObject();

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($mock);
        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($mock, $result);
    }

    public function testFetchList()
    {
        $ids = [1,2,3];
        $mock = array();

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1)->willReturn($mock);
        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->fetchList($ids);
        $this->assertEquals($mock, $result);
    }

    public function testSearch()
    {
        $filter = array();
        $sort = array();
        $number = 1;
        $size = 10;

        $mock = array();

        $adapter = $this->prophesize(IFetchAbleAdapter::class);
        $adapter->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($number),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn($mock);
        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->search($filter, $sort, $number, $size);

        $this->assertEquals($mock, $result);
    }
}
