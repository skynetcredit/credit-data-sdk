<?php
namespace Qxy\CreditData\Common\Repository;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullRepositoryTest extends TestCase
{
    private $contract;

    public function setUp()
    {
        $this->contract = NullRepository::getInstance();
    }

    public function tearDown()
    {
        unset($this->contract);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->contract
        );
    }

    public function testGetActualAdapter()
    {
        $result = $this->contract->getActualAdapter();
        $this->assertFalse($result);
        $this->assertEquals(TRANSLATOR_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetMockAdapter()
    {
        $result = $this->contract->getMockAdapter();
        $this->assertFalse($result);
        $this->assertEquals(TRANSLATOR_NOT_EXIST, Core::getLastError()->getId());
    }
}
