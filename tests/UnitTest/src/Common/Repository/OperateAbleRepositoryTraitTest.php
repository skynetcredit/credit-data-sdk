<?php
namespace Qxy\CreditData\Common\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Qxy\CreditData\Common\Model\MockObject;
use Qxy\CreditData\Common\Adapter\IOperateAbleAdapter;

class OperateAbleRepositoryTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(TestOperateAbleRepository::class)
            ->setMethods(['getAdapter'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testAdd()
    {
        $mock = new MockObject();

        $adapter = $this->prophesize(IOperateAbleAdapter::class);
        $adapter->add(Argument::exact($mock))->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->add($mock);
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $mock = new MockObject();

        $adapter = $this->prophesize(IOperateAbleAdapter::class);
        $adapter->edit(Argument::exact($mock))->shouldBeCalledTimes(1)->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $result = $this->stub->edit($mock);
        $this->assertTrue($result);
    }
}
