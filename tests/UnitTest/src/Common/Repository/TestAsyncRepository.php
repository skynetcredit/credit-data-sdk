<?php
namespace Qxy\CreditData\Common\Repository;

use Marmot\Interfaces\IAsyncAdapter;

class TestAsyncRepository implements IAsyncAdapter
{
    use AsyncRepositoryTrait;
}
