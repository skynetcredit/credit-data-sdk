<?php
namespace Qxy\CreditData\Common\Utils;

use Qxy\CreditData\Common\Model\IEnableAble;

class MockFactory
{
    public static function generateCreateTime($faker, array $value = array())
    {
        return isset($value['createTime']) ? $value['createTime'] : $faker->unixTime();
    }

    public static function generateUpdateTime($faker, array $value = array())
    {
        return isset($value['updateTime']) ? $value['updateTime'] : $faker->unixTime();
    }

    public static function generateStatusTime($faker, array $value = array())
    {
        return isset($value['statusTime']) ? $value['statusTime'] : $faker->unixTime();
    }

    public static function generateStatus($faker, array $value = array())
    {
        return isset($value['status']) ?
        $value['status'] : $faker->randomNumber(2);
    }
}
