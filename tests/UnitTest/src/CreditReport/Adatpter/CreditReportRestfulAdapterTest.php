<?php
namespace Qxy\CreditData\CreditReport\Adapter;

use Marmot\Interfaces\IRestfulTranslator;
use PHPUnit\Framework\TestCase;

use Qxy\CreditData\CreditReport\Model\NullCreditReport;
use Qxy\CreditData\CreditReport\Model\CreditReport;
use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;
use Qxy\CreditData\CreditReport\Translator\CreditReportRestfulTranslator;
use Qxy\CreditData\CreditReport\Utils\MockFactory;

/**
 * @SuppressWarnings(PHPMD)
 */
class CreditReportRestfulAdapterTest extends TestCase
{
    use CommonMapErrorsTrait;

    private $adapter;

    private $mockAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(CreditReportRestfulAdapter::class)
                           ->setMethods([
                            'getAdapter',
                            'fetchOneAction',
                            'getTranslator',
                            'post',
                            'patch',
                            'isSuccess',
                            'translateToObject',
                            'fetchCreditReportAction',
                            'get'
                           ])->getMock();

        $this->mockAdapter = new class extends CreditReportRestfulAdapter{
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }

            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->mockAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testICreditReportAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Adapter\ICreditReportAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->mockAdapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Translator\CreditReportRestfulTranslator',
            $this->mockAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals(
            'creditReports',
            $this->mockAdapter->getResource()
        );
    }

    public function testGetMapErrors()
    {
        $this->assertEquals(
            $this->commonMapErrors(),
            $this->mockAdapter->getMapErrors()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->mockAdapter->scenario($expect);
        $this->assertEquals($actual, $this->mockAdapter->getScenario());
    }

    public function scenarioDataProvider()
    {
        return [
            ['CREDIT_REPORT_LIST', CreditReportRestfulAdapter::SCENARIOS['CREDIT_REPORT_LIST']],
            ['CREDIT_REPORT_FETCH_ONE', CreditReportRestfulAdapter::SCENARIOS['CREDIT_REPORT_FETCH_ONE']],
            ['NULL', []]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $creditReport = MockFactory::generateCreditReportObject($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullCreditReport())
            ->willReturn($creditReport);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($creditReport, $result);
    }
    /**
     * 为CreditReportRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$creditReport，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareCreditReportTranslator(
        CreditReport $creditReport,
        array $keys,
        array $creditReportArray
    ) {
        $translator = $this->prophesize(CreditReportRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($creditReport),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($creditReportArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(CreditReport $creditReport)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($creditReport);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }

    public function testFetchCreditReport()
    {
        $subject = 'string';
        $identify = '123';
        $scoringModelId = 1;

        $this->adapter->expects($this->exactly(1))
            ->method('fetchCreditReportAction')
            ->with($subject, $identify, $scoringModelId)
            ->willReturn(true);

        $result = $this->adapter->fetchCreditReport($subject, $identify, $scoringModelId);

        $this->assertTrue($result);
    }
}
