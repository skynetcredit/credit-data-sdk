<?php
namespace Qxy\CreditData\CreditReport\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;
use Qxy\CreditData\Enterprise\Model\Enterprise;

/**
 * @SuppressWarnings(PHPMD)
 */
class CreditReportTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CreditReportDictionary::class)
            ->setMethods([
                'getRepository'
            ])->getMock();
        $this->subject = $this->getMockBuilder(
            'Qxy\CreditData\CreditReport\Model\CreditReport'
        )->getMockForAbstractClass();
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->subject);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->subject);
    }

    public function testSubjectConstructor()
    {
        $this->assertEquals(0, $this->subject->getId());
        $this->assertEmpty($this->subject->getScore());
        $this->assertEmpty($this->subject->getSourceRubric());

        $this->assertEquals(0, $this->subject->getStatus());
        $this->assertEquals(0, $this->subject->getStatusTime());
        $this->assertEquals(Core::$container->get('time'), $this->subject->getCreateTime());
        $this->assertEquals(Core::$container->get('time'), $this->subject->getUpdateTime());
    }

    public function testSetId()
    {
        $this->subject->setId(1);
        $this->assertEquals(1, $this->subject->getId());
    }

    //name 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setScore() 正确的传参类型,期望传值正确
     */
    public function testSetScoreCorrectType()
    {
        $this->subject->setScore('string');
        $this->assertEquals('string', $this->subject->getScore());
    }

    /**
     * 设置 Subject setScore() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetScoreWrongType()
    {
        $this->subject->setScore(array(1,2,3));
    }

    //name 测试 ---------------------------------------------------   end

    //identify 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setSourceRubric() 正确的传参类型,期望传值正确
     */
    public function testSetSourceRubricCorrectType()
    {
        $this->subject->setSourceRubric(array(1,2,3));
        $this->assertEquals(array(1,2,3), $this->subject->getSourceRubric());
    }

    /**
     * 设置 Subject setSourceRubric() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceRubricWrongType()
    {
        $this->subject->setSourceRubric('string');
    }
    //identify 测试 ---------------------------------------------------   end
    
    //enterprise 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setSourceRubric() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseRubricCorrectType()
    {
        $this->subject->setEnterprise(new Enterprise());
        $this->assertEquals(new Enterprise(), $this->subject->getEnterprise());
    }

    /**
     * 设置 Subject setEnterpriseRubric() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseRubricWrongType()
    {
        $this->subject->setEnterprise('string');
    }
    //enterprise 测试 ---------------------------------------------------   end


    //status 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->subject->setStatus(0);
        $this->assertEquals(0, $this->subject->getStatus());
    }

    /**
     * 设置 Subject setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->subject->setStatus('string');
    }
    //status 测试 ---------------------------------------------------   end
}
