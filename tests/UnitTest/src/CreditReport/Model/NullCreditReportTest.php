<?php
namespace Qxy\CreditData\CreditReport\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullCreditReportTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullCreditReport::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCreditReport()
    {
        $this->assertInstanceof(
            'Qxy\CreditData\CreditReport\Model\CreditReport',
            $this->stub
        );
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
