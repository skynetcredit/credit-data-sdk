<?php
namespace Qxy\CreditData\CreditReport\Repository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Qxy\CreditData\CreditReport\Adapter\CreditReportRestfulAdapter;
use Qxy\CreditData\CreditReport\Adapter\ICreditReportAdapter;

class CreditReportRepositoryTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            CreditReportRepository::class
        )
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends CreditReportRepository {
            public function getAdapter() : ICreditReportAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : ICreditReportAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Adapter\CreditReportRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Adapter\ICreditReportAdapter',
            $this->childStub->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Adapter\CreditReportRestfulAdapter',
            $this->childStub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Adapter\ICreditReportAdapter',
            $this->childStub->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditReport\Adapter\CreditReportMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $adapter = $this->prophesize(CreditReportRestfulAdapter::class);
        $adapter->scenario(
            Argument::exact(CreditReportRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(CreditReportRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    public function testFetchCreditReport()
    {
        $subject = 'string';
        $identify = '123';
        $scoringModelId = 1;

        $adapter = $this->prophesize(ICreditReportAdapter::class);
        $adapter->fetchCreditReport(
            Argument::exact($subject),
            Argument::exact($identify),
            Argument::exact($scoringModelId)
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());

        $this->stub->fetchCreditReport($subject, $identify, $scoringModelId);
    }
}
