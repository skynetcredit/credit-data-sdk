<?php
namespace Qxy\CreditData\CreditReport\Translator;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\CreditReport\Model\CreditReport;
use Qxy\CreditData\CreditReport\Utils\MockFactory;
use Qxy\CreditData\Enterprise\Translator\EnterpriseRestfulTranslator;
use Qxy\CreditData\Enterprise\Model\Enterprise;

class CreditReportRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub =
            $this->getMockBuilder(CreditReportRestfulTranslator::class)
                ->setMethods()
                ->getMock();
    }

    public function testGetEnterpriseRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\Enterprise\Translator\EnterpriseRestfulTranslator',
            $this->stub->getEnterpriseRestfulTranslator()
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->stub->arrayToObject(array());
        $this->assertInstanceOf('Qxy\CreditData\CreditReport\Model\NullCreditReport', $result);
    }

    public function setMethods(CreditReport $creditReport, array $attributes)
    {
        unset($relationships);

        if (isset($attributes['score'])) {
            $creditReport->setScore($attributes['score']);
        }
        if (isset($attributes['sourceRubric'])) {
            $creditReport->setSourceRubric($attributes['sourceRubric']);
        }

        if (isset($attributes['status'])) {
            $creditReport->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $creditReport->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $creditReport->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $creditReport->setStatusTime($attributes['statusTime']);
        }
        if (isset($relationships['subject']['data'])) {
            $creditReport->setEnterprise(new Enterprise($relationships['subject']['data']['id']));
        }

        return $creditReport;
    }

    public function testArrayToObjectCorrectObject()
    {
        $creditReport = \Qxy\CreditData\CreditReport\Utils\MockFactory::generateCreditReportArray();
        
        $data =  $creditReport['data'];

        $actual = $this->stub->arrayToObject($creditReport);
        $expectObject = new CreditReport();

        $expectObject->setScore($data['attributes']['score']);
        $expectObject->setSourceRubric($data['attributes']['sourceRubric']);

        $expectObject->setId($data['id']);
        
        $attributes = $data['attributes'];

        $expectObject = $this->setMethods($expectObject, $attributes);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObject()
    {
        $result = $this->stub->objectToArray(array());
        $this->assertEquals([], $result);
    }
}
