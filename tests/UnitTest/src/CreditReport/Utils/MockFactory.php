<?php
namespace Qxy\CreditData\CreditReport\Utils;

use Qxy\CreditData\CreditReport\Model\CreditReport;

class MockFactory
{
    public static function generateCreditReportArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $dictionary = array(
            'data'=>array(
                'type'=>'creditReports',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //score
        $score = self::generateScore($value);
        $attributes['score'] = $score;
        //sourceRubric
        $sourceRubric = self::generateSourceRubric($value);
        $attributes['sourceRubric'] = $sourceRubric;

        //createTime
        $createTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $dictionary['data']['attributes'] = $attributes;

        return $dictionary;
    }

    private static function generateScore(array $value = array())
    {
        return isset($value['score']) ? $value['score'] : 0;
    }

    private static function generateSourceRubric(array $value = array())
    {
        return isset($value['sourceRubric']) ? $value['sourceRubric'] : array();
    }

    public static function generateCreditReportObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CreditReport {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $dictionary = new CreditReport($id);

        //score
        $score = self::generateScore($value);
        $dictionary->setScore($score);
        //sourceRubric
        $sourceRubric = self::generateSourceRubric($value);
        $dictionary->setSourceRubric($sourceRubric);

        //createTime
        $dictionaryCreateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $dictionary->setCreateTime($dictionaryCreateTime);
        //updateTime
        $dictionaryUpdateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $dictionary->setUpdateTime($dictionaryUpdateTime);
        //statusTime
        $dictionaryStatusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $dictionary->setStatusTime($dictionaryStatusTime);
        //status
        $dictionaryStatus = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $dictionary->setStatus($dictionaryStatus);

        return $dictionary;
    }
}
