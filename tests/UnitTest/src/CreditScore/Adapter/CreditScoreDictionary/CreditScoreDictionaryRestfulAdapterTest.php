<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary;

use Marmot\Interfaces\IRestfulTranslator;
use PHPUnit\Framework\TestCase;
use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\NullCreditScoreDictionary;
use Qxy\CreditData\CreditScore\Utils\CreditScoreDictionary\MockFactory;

use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;

class CreditScoreDictionaryRestfulAdapterTest extends TestCase
{
    use CommonMapErrorsTrait;

    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CreditScoreDictionaryRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends CreditScoreDictionaryRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }

            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsILabelAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\ICreditScoreDictionaryAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('dictionaries', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Translator\CreditScoreDictionary\CreditScoreDictionaryRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    public function testGetMapErrors()
    {
        $this->assertEquals(
            $this->commonMapErrors(),
            $this->childStub->getMapErrors()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }

    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'CREDIT_SCORE_DICTIONARY_LIST',
                CreditScoreDictionaryRestfulAdapter::SCENARIOS['CREDIT_SCORE_DICTIONARY_LIST']
            ],
            [
                'CREDIT_SCORE_DICTIONARY_FETCH_ONE',
                CreditScoreDictionaryRestfulAdapter::SCENARIOS['CREDIT_SCORE_DICTIONARY_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $creditScoreDictionary = MockFactory::generateDictionaryObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullCreditScoreDictionary())
            ->willReturn($creditScoreDictionary);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($creditScoreDictionary, $result);
    }
}
