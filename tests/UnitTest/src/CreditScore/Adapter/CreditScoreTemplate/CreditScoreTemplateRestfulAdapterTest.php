<?php
namespace Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate;

use Marmot\Interfaces\IRestfulTranslator;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\NullCreditScoreTemplate;
use Qxy\CreditData\CreditScore\Translator\CreditScoreTemplate\CreditScoreTemplateRestfulTranslator;
use Qxy\CreditData\CreditScore\Utils\CreditScoreTemplate\MockFactory;

use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;

/**
 * @SuppressWarnings(PHPMD)
 */
class CreditScoreTemplateRestfulAdapterTest extends TestCase
{
    use CommonMapErrorsTrait;

    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CreditScoreTemplateRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends CreditScoreTemplateRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }

            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsICreditScoreTemplateAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('scoringModels', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Translator\CreditScoreTemplate\CreditScoreTemplateRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    public function testGetMapErrors()
    {
        $this->assertEquals(
            $this->commonMapErrors(),
            $this->childStub->getMapErrors()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }

    /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'CREDIT_SCORE_TEMPLATE_LIST',
                CreditScoreTemplateRestfulAdapter::SCENARIOS['CREDIT_SCORE_TEMPLATE_LIST']
            ],
            [
                'CREDIT_SCORE_TEMPLATE_FETCH_ONE',
                CreditScoreTemplateRestfulAdapter::SCENARIOS['CREDIT_SCORE_TEMPLATE_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOne()
    {
        $id = 1;

        $creditScoreTemplate = MockFactory::generateTemplateObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullCreditScoreTemplate())
            ->willReturn($creditScoreTemplate);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($creditScoreTemplate, $result);
    }

    /**
     * 为CreditScoreTemplateRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$label，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareCreditScoreTemplateTranslator(
        CreditScoreTemplate $creditScoreTemplate,
        array $keys,
        array $creditScoreTemplateArray
    ) {
        $translator = $this->prophesize(CreditScoreTemplateRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($creditScoreTemplate),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($creditScoreTemplateArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(CreditScoreTemplate $creditScoreTemplate)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($creditScoreTemplate);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditScoreTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $template = MockFactory::generateTemplateObject(1);
        $templateArray = array();

        $this->prepareCreditScoreTemplateTranslator(
            $template,
            array(
               'name',
               'applicationObject',
               'dataDomain',
               'dataSourceAndIndex',
            ),
            $templateArray
        );

        $this->stub->expects($this->exactly(1))
           ->method('post')
           ->with('scoringModels', $templateArray);

        $this->success($template);

        $result = $this->stub->add($template);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditScoreTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $template = MockFactory::generateTemplateObject(1);
        $templateArray = array();

        $this->prepareCreditScoreTemplateTranslator(
            $template,
            array(
                'name',
                'applicationObject',
                'dataDomain',
                'dataSourceAndIndex',
            ),
            $templateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('scoringModels', $templateArray);

        $this->failure($template);
        $result = $this->stub->add($template);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareCreditScoreTemplateTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $template = MockFactory::generateTemplateObject(1);
        $templateArray = array();

        $this->prepareCreditScoreTemplateTranslator(
            $template,
            array(
                'name',
                'applicationObject',
                'dataDomain',
                'dataSourceAndIndex',
            ),
            $templateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'scoringModels/'.$template->getId(),
                $templateArray
            );

        $this->success($template);

        $result = $this->stub->edit($template);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行preparelabelTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $template = MockFactory::generateTemplateObject(1);
        $templateArray = array();

        $this->prepareCreditScoreTemplateTranslator(
            $template,
            array(
                'name',
                'applicationObject',
                'dataDomain',
                'dataSourceAndIndex',
            ),
            $templateArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'scoringModels/'.$template->getId(),
                $templateArray
            );

        $this->failure($template);
        $result = $this->stub->edit($template);
        $this->assertFalse($result);
    }
}
