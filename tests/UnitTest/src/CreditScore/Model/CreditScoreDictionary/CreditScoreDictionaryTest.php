<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreDictionary;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\CreditScore\Repository\CreditScoreDictionary\CreditScoreDictionaryRepository;

/**
 * @SuppressWarnings(PHPMD)
 */
class CreditScoreDictionaryTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CreditScoreDictionary::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends CreditScoreDictionary{
            public function getRepository() : CreditScoreDictionaryRepository
            {
                return parent::getRepository();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Repository\CreditScoreDictionary\CreditScoreDictionaryRepository',
            $this->childStub->getRepository()
        );
    }

    public function testSetCreditScoreDictionaryIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    public function testSetCreditScoreDictionaryIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }

    public function testSetCreditScoreDictionaryNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 CreditScoreDictionary setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreDictionaryNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }

    public function testSetCreditScoreDictionaryParentIdCorrectType()
    {
        $this->stub->setParentId(1);
        $this->assertEquals(1, $this->stub->getParentId());
    }

    /**
     * 设置 CreditScoreDictionary setParentId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreDictionaryParentIdWrongType()
    {
        $this->stub->setParentId('string');
    }

    public function testSetCreditScoreDictionaryCategoryCorrectType()
    {
        $this->stub->setCategory(1);
        $this->assertEquals(1, $this->stub->getCategory());
    }

    /**
     * 设置 CreditScoreDictionary setParentId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreDictionaryCategoryWrongType()
    {
        $this->stub->setParentId('string');
    }

    //status 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 Subject setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 ---------------------------------------------------   end
}
