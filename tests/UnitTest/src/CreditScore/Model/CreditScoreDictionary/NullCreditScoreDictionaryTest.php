<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreDictionary;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullCreditScoreDictionaryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullCreditScoreDictionary::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCreditScoreDictionary()
    {
        $this->assertInstanceof(
            'Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\CreditScoreDictionary',
            $this->stub
        );
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
