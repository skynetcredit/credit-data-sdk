<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreTemplate;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter;
use Qxy\CreditData\Common\Adapter\IOperateAbleAdapter;

/**
 * @SuppressWarnings(PHPMD)
 */
class CreditScoreTemplateTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CreditScoreTemplate::class)
            ->setMethods([
                'getRepository'
            ])->getMock();

        $this->childStub = new Class extends CreditScoreTemplate{
            public function getRepository() : ICreditScoreTemplateAdapter
            {
                return parent::getRepository();
            }

            public function getIOperateAbleAdapter() : IOperateAbleAdapter
            {
                return parent::getIOperateAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Repository\CreditScoreTemplate\CreditScoreTemplateRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIOperateAbleAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\Common\Adapter\IOperateAbleAdapter',
            $this->childStub->getIOperateAbleAdapter()
        );
    }

    public function testSetCreditScoreTemplateIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    public function testSetCreditScoreTemplateIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertEquals(1, $this->stub->getId());
    }

    public function testSetCreditScoreTemplateNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 CreditScoreTemplate setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreTemplateNameWrongType()
    {
        $this->stub->setName(array(1, 2, 3));
    }

    public function testSetCreditScoreTemplateApplicationObjecCorrectType()
    {
        $this->stub->setApplicationObject(1);
        $this->assertEquals(1, $this->stub->getApplicationObject());
    }

    /**
     * 设置 CreditScoreTemplate setApplicationObject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreTemplateApplicationObjecWrongType()
    {
        $this->stub->setApplicationObject('string');
    }

    public function testSetCreditScoreTemplateCategoryCorrectType()
    {
        $this->stub->setDataDomain(array(1,2));
        $this->assertEquals(array(1,2), $this->stub->getDataDomain());
    }

    /**
     * 设置 CreditScoreTemplate setDataDomain() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreTemplateCategoryWrongType()
    {
        $this->stub->setDataDomain('string');
    }

    public function testSetCreditScoreTemplateDataSourceAndIndexCorrectType()
    {
        $this->stub->setDataSourceAndIndex(array(array()));
        $this->assertEquals(array(array()), $this->stub->getDataSourceAndIndex());
    }

    /**
     * 设置 CreditScoreTemplate setDataSourceAndIndex() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCreditScoreTemplateDataSourceAndIndexWrongType()
    {
        $this->stub->setDataSourceAndIndex('string');
    }

    //status 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 Subject setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 ---------------------------------------------------   end
}
