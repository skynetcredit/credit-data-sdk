<?php
namespace Qxy\CreditData\CreditScore\Model\CreditScoreTemplate;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullCreditScoreTemplateTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullCreditScoreTemplate::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);

        $this->childStub = new class extends NullCreditScoreTemplate{
            public function resourceNotExist() : bool
            {
                return parent::resourceNotExist();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsCreditScoreTemplate()
    {
        $this->assertInstanceof(
            'Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate',
            $this->stub
        );
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testResourceNotExist()
    {
        $result = $this->childStub->resourceNotExist();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
