<?php
namespace Qxy\CreditData\CreditScore\Repository\CreditScoreDictionary;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\CreditScoreDictionaryRestfulAdapter;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\ICreditScoreDictionaryAdapter;

class CreditScoreDictionaryRepositoryTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            CreditScoreDictionaryRepository::class
        )
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends CreditScoreDictionaryRepository {
            public function getAdapter() : ICreditScoreDictionaryAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : ICreditScoreDictionaryAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\CreditScoreDictionaryRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\ICreditScoreDictionaryAdapter',
            $this->childStub->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\CreditScoreDictionaryRestfulAdapter',
            $this->childStub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\ICreditScoreDictionaryAdapter',
            $this->childStub->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreDictionary\CreditScoreDictionaryMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $adapter = $this->prophesize(CreditScoreDictionaryRestfulAdapter::class);
        $adapter->scenario(
            Argument::exact(CreditScoreDictionaryRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(CreditScoreDictionaryRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
