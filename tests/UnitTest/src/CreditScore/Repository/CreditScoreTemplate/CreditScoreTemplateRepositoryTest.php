<?php
namespace Qxy\CreditData\CreditScore\Repository\CreditScoreTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateRestfulAdapter;
use Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter;

class CreditScoreTemplateRepositoryTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            CreditScoreTemplateRepository::class
        )
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends CreditScoreTemplateRepository {
            public function getAdapter() : ICreditScoreTemplateAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : ICreditScoreTemplateAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter',
            $this->childStub->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateRestfulAdapter',
            $this->childStub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\ICreditScoreTemplateAdapter',
            $this->childStub->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Adapter\CreditScoreTemplate\CreditScoreTemplateMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $adapter = $this->prophesize(CreditScoreTemplateRestfulAdapter::class);
        $adapter->scenario(Argument::exact(CreditScoreTemplateRepository::FETCH_ONE_MODEL_UN))->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(CreditScoreTemplateRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }
}
