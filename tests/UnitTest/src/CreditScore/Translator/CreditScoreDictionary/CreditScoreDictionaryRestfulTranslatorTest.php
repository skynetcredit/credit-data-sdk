<?php
namespace Qxy\CreditData\CreditScore\Translator\CreditScoreDictionary;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\CreditScoreDictionary;
use Qxy\CreditData\CreditScore\Utils\CreditScoreDictionary\MockFactory;

class CreditScoreDictionaryRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub =
            $this->getMockBuilder(CreditScoreDictionaryRestfulTranslator::class)
                ->setMethods()
                ->getMock();
    }

    public function setMethods(CreditScoreDictionary $dictionary, array $attributes, array $relationships)
    {
        unset($relationships);

        if (isset($attributes['name'])) {
            $dictionary->setName($attributes['name']);
        }
        if (isset($attributes['parentId'])) {
            $dictionary->setParentId($attributes['parentId']);
        }
        if (isset($attributes['category'])) {
            $dictionary->setCategory($attributes['category']);
        }
        if (isset($attributes['status'])) {
            $dictionary->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $dictionary->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $dictionary->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $dictionary->setStatusTime($attributes['statusTime']);
        }

        return $dictionary;
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(
            array(),
            new CreditScoreDictionary()
        );

        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\NullCreditScoreDictionary',
            $result
        );
    }

    public function testArrayToObjectCorrectObject()
    {
        $dictionary = MockFactory::generateDictionaryArray();

        $datas =  $dictionary['data'];
        $relationships = array();

        $actual = $this->stub->arrayToObject($dictionary);

        $expectObject = new CreditScoreDictionary();

        $expectObject->setId($datas['id']);

        $attributes = isset($datas['attributes']) ? $datas['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }
}
