<?php
namespace Qxy\CreditData\CreditScore\Translator\CreditScoreTemplate;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;
use Qxy\CreditData\CreditScore\Utils\CreditScoreTemplate\MockFactory;

class CreditScoreTemplateRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub =
            $this->getMockBuilder(CreditScoreTemplateRestfulTranslator::class)
                ->setMethods()
                ->getMock();
    }

    /**
     * @SuppressWarnings(PHPMD)
     */
    public function setMethods(CreditScoreTemplate $dictionary, array $attributes, array $relationships)
    {
        unset($relationships);

        if (isset($attributes['name'])) {
            $dictionary->setName($attributes['name']);
        }
        if (isset($attributes['applicationObject'])) {
            $dictionary->setApplicationObject($attributes['applicationObject']);
        }
        if (isset($attributes['dataDomain'])) {
            $dictionary->setDataDomain($attributes['dataDomain']);
        }
        if (isset($attributes['dataSourceAndIndex'])) {
            $dictionary->setDataSourceAndIndex($attributes['dataSourceAndIndex']);
        }
        if (isset($attributes['status'])) {
            $dictionary->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $dictionary->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $dictionary->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $dictionary->setStatusTime($attributes['statusTime']);
        }

        return $dictionary;
    }

    public function testArrayToObjectIncorrectObject()
    {
        $result = $this->stub->arrayToObject(
            array(),
            new CreditScoreTemplate()
        );

        $this->assertInstanceOf(
            'Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\NullCreditScoreTemplate',
            $result
        );
    }

    public function testArrayToObjectCorrectObject()
    {
        $dictionary = MockFactory::generateTemplateArray();

        $datas =  $dictionary['data'];
        $relationships = array();

        $actual = $this->stub->arrayToObject($dictionary);

        $expectObject = new CreditScoreTemplate();

        $expectObject->setId($datas['id']);

        $attributes = isset($datas['attributes']) ? $datas['attributes'] : '';

        $expectObject = $this->setMethods($expectObject, $attributes, $relationships);

        $this->assertEquals($expectObject, $actual);
    }
}
