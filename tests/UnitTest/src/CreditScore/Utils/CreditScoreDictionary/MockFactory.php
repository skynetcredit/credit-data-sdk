<?php
namespace Qxy\CreditData\CreditScore\Utils\CreditScoreDictionary;

use Qxy\CreditData\CreditScore\Model\CreditScoreDictionary\CreditScoreDictionary;

class MockFactory
{
    public static function generateDictionaryArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $dictionary = array(
            'data'=>array(
                'type'=>'dictionaries',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //category
        $category = self::generateCategory($faker, $value);
        $attributes['category'] = $category;
        //parentId
        $parentId = self::generateParentId($faker, $value);
        $attributes['parentId'] = $parentId;

        //statusTime
        $statusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;
        //updateTime
        $updateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;

        //createTime
        $createTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;

        $dictionary['data']['attributes'] = $attributes;

        return $dictionary;
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    private static function generateParentId($faker, array $value = array())
    {
        return isset($value['parentId']) ? $value['parentId'] : $faker->randomNumber();
    }

    private static function generateCategory($faker, array $value = array())
    {
        return isset($value['category'])
            ? $value['category']
            : $faker->randomElement(CreditScoreDictionary::CREDIT_SCORE_DICTIONARY_CATEGORY);
    }

    public static function generateDictionaryObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CreditScoreDictionary {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $dictionary = new CreditScoreDictionary($id);

        //name
        $name = self::generateName($faker, $value);
        $dictionary->setName($name);
        //category
        $category = self::generateCategory($faker, $value);
        $dictionary->setCategory($category);
        //parentId
        $parentId = self::generateCategory($faker, $value);
        $dictionary->setParentId($parentId);
        //createTime
        $dictionaryCreateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $dictionary->setCreateTime($dictionaryCreateTime);
        //updateTime
        $dictionaryUpdateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $dictionary->setUpdateTime($dictionaryUpdateTime);
        //statusTime
        $dictionaryStatusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $dictionary->setStatusTime($dictionaryStatusTime);
        //status
        $dictionaryStatus = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $dictionary->setStatus($dictionaryStatus);

        return $dictionary;
    }
}
