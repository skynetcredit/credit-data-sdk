<?php
namespace Qxy\CreditData\CreditScore\Utils\CreditScoreTemplate;

use Qxy\CreditData\CreditScore\Model\CreditScoreTemplate\CreditScoreTemplate;

class MockFactory
{
    public static function generateTemplateArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $dictionary = array(
            'data'=>array(
                'type'=>'scoringModels',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //applicationObject
        $applicationObject = self::generateApplicationObject($faker, $value);
        $attributes['applicationObject'] = $applicationObject;
        //dataDomain
        $dataDomain = self::generateDataDomain($faker, $value);
        $attributes['dataDomain'] = $dataDomain;
        //dataSourceAndIndex
        $dataSourceAndIndex = self::generateDataSourceAndIndex($faker, $value);
        $attributes['dataSourceAndIndex'] = $dataSourceAndIndex;
        // createTime
        $templateCreateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $templateCreateTime;
        // updateTime
        $templateUpdateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $templateUpdateTime;
        // statusTime
        $templateStatusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $templateStatusTime;
        // status
        $templateStatus = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $templateStatus;

        $dictionary['data']['attributes'] = $attributes;

        return $dictionary;
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    private static function generateDataDomain($faker, array $value = array())
    {
        unset($faker);
        return isset($value['dataDomain'])
            ? $value['dataDomain']
            : array(1,2);
    }

    private static function generateDataSourceAndIndex($faker, array $value = array())
    {
        unset($faker);
        return isset($value['dataSourceAndIndex'])
            ? $value['dataSourceAndIndex']
            : array(
                array()
            );
    }

    private static function generateApplicationObject($faker, array $value = array())
    {
        return isset($value['applicationObject'])
            ? $value['applicationObject']
            : $faker->randomNumber();
    }

    public static function generateTemplateObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CreditScoreTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $template = new CreditScoreTemplate($id);

        //name
        $name = self::generateName($faker, $value);
        $template->setName($name);
        //applicationObject
        $applicationObject = self::generateApplicationObject($faker, $value);
        $template->setApplicationObject($applicationObject);
        //dataDomain
        $dataDomain = self::generateDataDomain($faker, $value);
        $template->setDataDomain($dataDomain);
        //dataSourceAndIndex
        $dataSourceAndIndex = self::generateDataSourceAndIndex($faker, $value);
        $template->setDataSourceAndIndex($dataSourceAndIndex);
        //createTime
        $createTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $template->setCreateTime($createTime);
        //updateTime
        $updateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $template->setUpdateTime($updateTime);
        //statusTime
        $statusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $template->setStatusTime($statusTime);
        //status
        $status = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $template->setStatus($status);

        return $template;
    }
}
