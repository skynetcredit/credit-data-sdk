<?php
namespace Qxy\CreditData\Enterprise\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class EnterpriseTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(EnterpriseDictionary::class)
            ->setMethods([
                'getRepository'
            ])->getMock();
        $this->subject = $this->getMockBuilder('Qxy\CreditData\Enterprise\Model\Enterprise')->getMockForAbstractClass();
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->subject);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->subject);
    }

    public function testSubjectConstructor()
    {
        $this->assertEquals(0, $this->subject->getId());
        $this->assertEmpty($this->subject->getName());
        $this->assertEmpty($this->subject->getIdentify());
        $this->assertEmpty($this->subject->getEnterpriseType());
        $this->assertEmpty($this->subject->getPrincipal());
        $this->assertEmpty($this->subject->getBusinessStatus());
        $this->assertEmpty($this->subject->getRegistrationAuthority());
        $this->assertEquals('0000-00-00', $this->subject->getEstablishedDate());

        $this->assertEquals(0, $this->subject->getStatus());
        $this->assertEquals(0, $this->subject->getStatusTime());
        $this->assertEquals(Core::$container->get('time'), $this->subject->getCreateTime());
        $this->assertEquals(Core::$container->get('time'), $this->subject->getUpdateTime());
    }

    public function testSetId()
    {
        $this->subject->setId(1);
        $this->assertEquals(1, $this->subject->getId());
    }

    //identify 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->subject->setIdentify('string');
        $this->assertEquals('string', $this->subject->getIdentify());
    }

    /**
     * 设置 Subject setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->subject->setIdentify(array(1,2,3));
    }
    //identify 测试 ---------------------------------------------------   end

    //status 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->subject->setStatus(0);
        $this->assertEquals(0, $this->subject->getStatus());
    }

    /**
     * 设置 Subject setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->subject->setStatus('string');
    }
    //status 测试 ---------------------------------------------------   end
    //
    
    //name 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->subject->setName('string');
        $this->assertEquals('string', $this->subject->getName());
    }

    /**
     * 设置 Subject setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->subject->setName(array(1,2,3));
    }

    //name 测试 ---------------------------------------------------   end
}
