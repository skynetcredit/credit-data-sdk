<?php
namespace Qxy\CreditData\Enterprise\Translator;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\Enterprise\Model\Enterprise;
use Qxy\CreditData\Enterprise\Utils\MockFactory;

/**
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub =
            $this->getMockBuilder(EnterpriseRestfulTranslator::class)
                ->setMethods()
                ->getMock();
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->stub->arrayToObject(array());
        $this->assertInstanceOf('Qxy\CreditData\Enterprise\Model\NullEnterprise', $result);
    }

    public function setMethods(Enterprise $enterprise, array $attributes)
    {
        if (isset($attributes['name'])) {
            $enterprise->setName($attributes['name']);
        }
        if (isset($attributes['unifiedSocialCreditCode'])) {
            $enterprise->setIdentify($attributes['unifiedSocialCreditCode']);
        }
        if (isset($attributes['enterpriseType'])) {
            $enterprise->setEnterpriseType($attributes['enterpriseType']);
        }
        if (isset($attributes['principal'])) {
            $enterprise->setPrincipal($attributes['principal']);
        }
        if (isset($attributes['businessStatus'])) {
            $enterprise->setBusinessStatus($attributes['businessStatus']);
        }
        if (isset($attributes['registrationAuthority'])) {
            $enterprise->setRegistrationAuthority($attributes['registrationAuthority']);
        }
        if (isset($attributes['establishedDate'])) {
            $enterprise->setEstablishedDate($attributes['establishedDate']);
        }

        if (isset($attributes['status'])) {
            $enterprise->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $enterprise->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $enterprise->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $enterprise->setStatusTime($attributes['statusTime']);
        }

        return $enterprise;
    }

    public function testArrayToObjectCorrectObject()
    {
        $enterprise = \Qxy\CreditData\Enterprise\Utils\MockFactory::generateDictionaryArray();
        
        $data =  $enterprise['data'];

        $actual = $this->stub->arrayToObject($enterprise);
        $expectObject = new Enterprise();

        $expectObject->setName($data['attributes']['name']);
        $expectObject->setIdentify($data['attributes']['unifiedSocialCreditCode']);
        $expectObject->setEnterpriseType($data['attributes']['enterpriseType']);
        $expectObject->setPrincipal($data['attributes']['principal']);
        $expectObject->setBusinessStatus($data['attributes']['businessStatus']);
        $expectObject->setRegistrationAuthority($data['attributes']['registrationAuthority']);
        $expectObject->setEstablishedDate($data['attributes']['establishedDate']);

        $expectObject->setId($data['id']);
        
        $attributes = $data['attributes'];

        $expectObject = $this->setMethods($expectObject, $attributes);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObject()
    {
        $result = $this->stub->objectToArray(array());
        $this->assertEquals([], $result);
    }
}
