<?php
namespace Qxy\CreditData\Enterprise\Utils;

use Qxy\CreditData\Enterprise\Model\Enterprise;

class MockFactory
{
    public static function generateDictionaryArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $dictionary = array(
            'data'=>array(
                'type'=>'enterprises',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //identify
        $identify = self::generateIdentify($value);
        $attributes['unifiedSocialCreditCode'] = $identify;
        //enterpriseType
        $enterpriseType = self::generateEnterpriseType($faker, $value);
        $attributes['enterpriseType'] = $enterpriseType;
        //principal
        $principal = self::generatePrincipal($faker, $value);
        $attributes['principal'] = $principal;
        //businessStatus
        $businessStatus = self::generateBusinessStatus($faker, $value);
        $attributes['businessStatus'] = $businessStatus;
        //registrationAuthority
        $registrationAuthority = self::generateRegistrationAuthority($faker, $value);
        $attributes['registrationAuthority'] = $registrationAuthority;
        //establishedDate
        $establishedDate = self::generateEstablishedDate($value);
        $attributes['establishedDate'] = $establishedDate;

        //updateTime
        $updateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;
        //createTime
        $createTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        
        $dictionary['data']['attributes'] = $attributes;

        return $dictionary;
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    private static function generateIdentify(array $value = array())
    {
        return isset($value['identify']) ? $value['identify'] : '';
    }

    private static function generateEnterpriseType($faker, array $value = array())
    {
        return isset($value['EnterpriseType'])
            ? $value['EnterpriseType']
            : $faker->name();
    }

    private static function generatePrincipal($faker, array $value = array())
    {
        return isset($value['principal'])
            ? $value['principal']
            : $faker->name();
    }

    private static function generateBusinessStatus($faker, array $value = array())
    {
        return isset($value['businessStatus'])
            ? $value['businessStatus']
            : $faker->name();
    }

    private static function generateRegistrationAuthority($faker, array $value = array())
    {
        return isset($value['registrationAuthority'])
            ? $value['registrationAuthority']
            : $faker->name();
    }

    private static function generateEstablishedDate(array $value = array())
    {
        return isset($value['EstablishedDate'])
            ? $value['EstablishedDate']
            : '0000-00-00';
    }

    public static function generateDictionaryObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Enterprise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $dictionary = new Enterprise($id);

        //name
        $name = self::generateName($faker, $value);
        $dictionary->setName($name);
        //identify
        $identify = self::generateIdentify($value);
        $dictionary->setIdentify($identify);
        //enterpriseType
        $enterpriseType = self::generateEnterpriseType($faker, $value);
        $dictionary->setEnterpriseType($enterpriseType);
        //principal
        $principal = self::generatePrincipal($faker, $value);
        $dictionary->setPrincipal($principal);
        //businessStatus
        $businessStatus = self::generateBusinessStatus($faker, $value);
        $dictionary->setBusinessStatus($businessStatus);
        //registrationAuthority
        $registrationAuthority = self::generateRegistrationAuthority($faker, $value);
        $dictionary->setRegistrationAuthority($registrationAuthority);
        //establishedDate
        $establishedDate = self::generateEstablishedDate($value);
        $dictionary->setEstablishedDate($establishedDate);


        //createTime
        $dictionaryCreateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $dictionary->setCreateTime($dictionaryCreateTime);
        //updateTime
        $dictionaryUpdateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $dictionary->setUpdateTime($dictionaryUpdateTime);
        //statusTime
        $dictionaryStatusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $dictionary->setStatusTime($dictionaryStatusTime);
        //status
        $dictionaryStatus = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $dictionary->setStatus($dictionaryStatus);

        return $dictionary;
    }
}
