<?php
namespace Qxy\CreditData\ResourceCatalog\Adapter;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;
use PHPUnit\Framework\TestCase;

use Qxy\CreditData\ResourceCatalog\Model\NullResourceCatalog;
use Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog;

use Qxy\CreditData\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Qxy\CreditData\Common\Adapter\CommonMapErrorsTrait;
use Qxy\CreditData\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Qxy\CreditData\ResourceCatalog\Translator\ResourceCatalogRestfulTranslator;
use Qxy\CreditData\ResourceCatalog\Utils\MockFactory;

/**
 * @SuppressWarnings(PHPMD)
 */
class ResourceCatalogRestfulAdapterTest extends TestCase
{
    use CommonMapErrorsTrait;

    private $adapter;

    private $mockAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ResourceCatalogRestfulAdapter::class)
                           ->setMethods([
                            'getAdapter',
                            'fetchOneAction',
                            'getTranslator',
                            'post',
                            'patch',
                            'isSuccess',
                            'translateToObject'
                           ])->getMock();

        $this->mockAdapter = new class extends ResourceCatalogRestfulAdapter{
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }

            public function getMapErrors() : array
            {
                return parent::getMapErrors();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->mockAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testIResourceCatalogAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\IResourceCatalogAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->mockAdapter->getTranslator()
        );

        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Translator\ResourceCatalogRestfulTranslator',
            $this->mockAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals(
            'resourceCatalogs',
            $this->mockAdapter->getResource()
        );
    }

    public function testGetMapErrors()
    {
        $this->assertEquals(
            $this->commonMapErrors(),
            $this->mockAdapter->getMapErrors()
        );
    }
    /**
     * 循环测试 scenario() 是否符合预定范围
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->mockAdapter->scenario($expect);
        $this->assertEquals($actual, $this->mockAdapter->getScenario());
    }

    public function scenarioDataProvider()
    {
        return [
            ['RESOURCE_CATALOG_LIST', ResourceCatalogRestfulAdapter::SCENARIOS['RESOURCE_CATALOG_LIST']],
            ['RESOURCE_CATALOG_FETCH_ONE', ResourceCatalogRestfulAdapter::SCENARIOS['RESOURCE_CATALOG_FETCH_ONE']],
            ['NULL', []]
        ];
    }

    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $resourceCatalog = MockFactory::generateResourceCatalogObject($id);

        $this->adapter->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullResourceCatalog())
            ->willReturn($resourceCatalog);

        $result = $this->adapter->fetchOne($id);
        $this->assertEquals($resourceCatalog, $result);
    }
    /**
     * 为ResourceCatalogRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$resourceCatalog，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareResourceCatalogTranslator(
        ResourceCatalog $resourceCatalog,
        array $keys,
        array $resourceCatalogArray
    ) {
        $translator = $this->prophesize(ResourceCatalogRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($resourceCatalog),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($resourceCatalogArray);

        $this->adapter->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(ResourceCatalog $resourceCatalog)
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->adapter->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($resourceCatalog);
    }

    private function failure()
    {
        $this->adapter->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->adapter->expects($this->exactly(0))
            ->method('translateToObject');
    }
}
