<?php
namespace Qxy\CreditData\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NullResourceCatalogTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullResourceCatalog::getInstance();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testExtendsResourceCatalog()
    {
        $this->assertInstanceof(
            'Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog',
            $this->stub
        );
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}
