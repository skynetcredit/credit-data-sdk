<?php
namespace Qxy\CreditData\ResourceCatalog\Model;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

/**
 * @SuppressWarnings(PHPMD)
 */
class ResourceCatalogTest extends TestCase
{
    private $stub;

    private $subject;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ResourceCatalogDictionary::class)
            ->setMethods([
                'getRepository'
            ])->getMock();
        $this->subject = $this->getMockBuilder(
            'Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog'
        )->getMockForAbstractClass();
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->subject);
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->subject);
    }

    public function testSubjectConstructor()
    {
        $this->assertEquals(0, $this->subject->getId());
        $this->assertEmpty($this->subject->getName());
        $this->assertEmpty($this->subject->getApplicationObject());
        $this->assertEmpty($this->subject->getDataDomain());
        $this->assertEmpty($this->subject->getDataSource());
        $this->assertEmpty($this->subject->getRule());
        $this->assertEquals(array(), $this->subject->getEnumOptions());
        $this->assertEquals(array(), $this->subject->getTemplate());

        $this->assertEquals(0, $this->subject->getStatus());
        $this->assertEquals(0, $this->subject->getStatusTime());
        $this->assertEquals(Core::$container->get('time'), $this->subject->getCreateTime());
        $this->assertEquals(Core::$container->get('time'), $this->subject->getUpdateTime());
    }

    public function testSetId()
    {
        $this->subject->setId(1);
        $this->assertEquals(1, $this->subject->getId());
    }

    //name 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->subject->setName('string');
        $this->assertEquals('string', $this->subject->getName());
    }

    /**
     * 设置 Subject setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->subject->setName(array(1,2,3));
    }

    //name 测试 ---------------------------------------------------   end

    //identify 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setApplicationObject() 正确的传参类型,期望传值正确
     */
    public function testSetApplicationObjectCorrectType()
    {
        $this->subject->setApplicationObject(0);
        $this->assertEquals(0, $this->subject->getApplicationObject());
    }

    /**
     * 设置 Subject setApplicationObject() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplicationObjectWrongType()
    {
        $this->subject->setApplicationObject(array(1,2,3));
    }
    //identify 测试 ---------------------------------------------------   end
    
    //dataDomain 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setApplicationObject() 正确的传参类型,期望传值正确
     */
    public function testSetDataDomainCorrectType()
    {
        $this->subject->setDataDomain(0);
        $this->assertEquals(0, $this->subject->getDataDomain());
    }

    /**
     * 设置 Subject setDataDomain() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataDomainWrongType()
    {
        $this->subject->setDataDomain(array(1,2,3));
    }
    //dataDomain 测试 ---------------------------------------------------   end
    
    //dataSource 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setApplicationObject() 正确的传参类型,期望传值正确
     */
    public function testSetDataSourceCorrectType()
    {
        $this->subject->setDataSource(0);
        $this->assertEquals(0, $this->subject->getDataSource());
    }

    /**
     * 设置 Subject setDataSource() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataSourceWrongType()
    {
        $this->subject->setDataSource(array(1,2,3));
    }
    //dataSource 测试 ---------------------------------------------------   end
    

    //rule 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setApplicationObject() 正确的传参类型,期望传值正确
     */
    public function testSetRuleCorrectType()
    {
        $this->subject->setRule(0);
        $this->assertEquals(0, $this->subject->getRule());
    }

    /**
     * 设置 Subject setRule() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRuleWrongType()
    {
        $this->subject->setRule(array(1,2,3));
    }
    //rule 测试 ---------------------------------------------------   end

    //status 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->subject->setStatus(0);
        $this->assertEquals(0, $this->subject->getStatus());
    }

    /**
     * 设置 Subject setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->subject->setStatus('string');
    }
    //status 测试 ---------------------------------------------------   end
    
    //enumOptions 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetEnumOptionsCorrectType()
    {
        $this->subject->setEnumOptions(array());
        $this->assertEquals(array(), $this->subject->getEnumOptions());
    }

    /**
     * 设置 Subject setEnumOptions() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnumOptionsWrongType()
    {
        $this->subject->setEnumOptions('string');
    }
    //enumOptions 测试 ---------------------------------------------------   end
    
    //template 测试 --------------------------------------------------- start
    /**
     * 设置 Subject setStatus() 正确的传参类型,期望传值正确
     */
    public function testSettemplateCorrectType()
    {
        $this->subject->settemplate(array());
        $this->assertEquals(array(), $this->subject->gettemplate());
    }

    /**
     * 设置 Subject settemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSettemplateWrongType()
    {
        $this->subject->settemplate('string');
    }
    //template 测试 ---------------------------------------------------   end
}
