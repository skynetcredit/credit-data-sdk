<?php
namespace Qxy\CreditData\ResourceCatalog\Repository;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Qxy\CreditData\ResourceCatalog\Adapter\ResourceCatalogRestfulAdapter;
use Qxy\CreditData\ResourceCatalog\Adapter\IResourceCatalogAdapter;

class ResourceCatalogRepositoryTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(
            ResourceCatalogRepository::class
        )
            ->setMethods(['getAdapter'])
            ->getMock();

        $this->childStub = new class extends ResourceCatalogRepository {
            public function getAdapter() : IResourceCatalogAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter()
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : IResourceCatalogAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->stub
        );
    }

    public function testExtendsIResourceCatalogAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\IResourceCatalogAdapter',
            $this->stub
        );
    }

    public function testGetAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\ResourceCatalogRestfulAdapter',
            $this->childStub->getAdapter()
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\IResourceCatalogAdapter',
            $this->childStub->getActualAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\ResourceCatalogRestfulAdapter',
            $this->childStub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\IResourceCatalogAdapter',
            $this->childStub->getMockAdapter()
        );
        $this->assertInstanceOf(
            'Qxy\CreditData\ResourceCatalog\Adapter\ResourceCatalogMockAdapter',
            $this->childStub->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $adapter = $this->prophesize(ResourceCatalogRestfulAdapter::class);
        $adapter->scenario(
            Argument::exact(ResourceCatalogRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
            ->method('getAdapter')
            ->willReturn($adapter->reveal());
        $result = $this->stub->scenario(ResourceCatalogRepository::FETCH_ONE_MODEL_UN);
        $this->assertEquals($this->stub, $result);
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IResourceCatalogAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->stub->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IResourceCatalogAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->stub->fetchList($ids);
    }

    public function testSearch()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IResourceCatalogAdapter::class);
        $adapter->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->stub->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->stub->search($filter, $sort, $offset, $size);
    }
}
