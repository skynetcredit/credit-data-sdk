<?php
namespace Qxy\CreditData\ResourceCatalog\Translator;

use PHPUnit\Framework\TestCase;
use Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog;
use Qxy\CreditData\ResourceCatalog\Utils\MockFactory;

/**
 * @SuppressWarnings(PHPMD)
 */
class ResourceCatalogRestfulTranslatorTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub =
            $this->getMockBuilder(ResourceCatalogRestfulTranslator::class)
                ->setMethods()
                ->getMock();
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->stub->arrayToObject(array());
        $this->assertInstanceOf('Qxy\CreditData\ResourceCatalog\Model\NullResourceCatalog', $result);
    }

    public function setMethods(ResourceCatalog $resourceCatalog, array $attributes)
    {
        if (isset($attributes['name'])) {
            $resourceCatalog->setName($attributes['name']);
        }
        if (isset($attributes['dataDomain'])) {
            $resourceCatalog->setApplicationObject($attributes['dataDomain']);
        }
        if (isset($attributes['dataDomain'])) {
            $resourceCatalog->setDataDomain($attributes['dataDomain']);
        }
        if (isset($attributes['dataSource'])) {
            $resourceCatalog->setDataSource($attributes['dataSource']);
        }
        if (isset($attributes['rule'])) {
            $resourceCatalog->setRule($attributes['rule']);
        }
        if (isset($attributes['enumOptions'])) {
            $resourceCatalog->setEnumOptions($attributes['enumOptions']);
        }
        if (isset($attributes['template'])) {
            $resourceCatalog->setTemplate($attributes['template']);
        }

        if (isset($attributes['status'])) {
            $resourceCatalog->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $resourceCatalog->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $resourceCatalog->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $resourceCatalog->setStatusTime($attributes['statusTime']);
        }

        return $resourceCatalog;
    }

    public function testArrayToObjectCorrectObject()
    {
        $resourceCatalog = \Qxy\CreditData\ResourceCatalog\Utils\MockFactory::generateResourceCatalogArray();
        
        $data =  $resourceCatalog['data'];

        $actual = $this->stub->arrayToObject($resourceCatalog);
        $expectObject = new ResourceCatalog();

        $expectObject->setName($data['attributes']['name']);
        $expectObject->setApplicationObject($data['attributes']['dataDomain']);
        $expectObject->setDataDomain($data['attributes']['dataDomain']);
        $expectObject->setDataSource($data['attributes']['dataSource']);
        $expectObject->setRule($data['attributes']['rule']);
        $expectObject->setEnumOptions($data['attributes']['enumOptions']);
        $expectObject->setTemplate($data['attributes']['template']);

        $expectObject->setId($data['id']);
        
        $attributes = $data['attributes'];

        $expectObject = $this->setMethods($expectObject, $attributes);

        $this->assertEquals($expectObject, $actual);
    }

    public function testArrayToObject()
    {
        $result = $this->stub->objectToArray(array());
        $this->assertEquals([], $result);
    }
}
