<?php
namespace Qxy\CreditData\ResourceCatalog\Utils;

use Qxy\CreditData\ResourceCatalog\Model\ResourceCatalog;

class MockFactory
{
    public static function generateResourceCatalogArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $dictionary = array(
            'data'=>array(
                'type'=>'enterprises',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();

        //name
        $name = self::generateName($faker, $value);
        $attributes['name'] = $name;
        //applicationObject
        $applicationObject = self::generateApplicationObject($value);
        $attributes['applicationObject'] = $applicationObject;
        //dataDomain
        $dataDomain = self::generateDataDomain($value);
        $attributes['dataDomain'] = $dataDomain;
        //dataSource
        $dataSource = self::generateDataSource($value);
        $attributes['dataSource'] = $dataSource;
        //rule
        $rule = self::generateRule($value);
        $attributes['rule'] = $rule;
        //enumOptions
        $enumOptions = self::generateEnumOptions($value);
        $attributes['enumOptions'] = $enumOptions;
        //template
        $template = self::generateTemplate($value);
        $attributes['template'] = $template;

        //createTime
        $createTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $dictionary['data']['attributes'] = $attributes;

        return $dictionary;
    }

    private static function generateName($faker, array $value = array())
    {
        return isset($value['name']) ? $value['name'] : $faker->name();
    }

    private static function generateApplicationObject(array $value = array())
    {
        return isset($value['applicationObject']) ? $value['applicationObject'] : 0;
    }

    private static function generateDataDomain(array $value = array())
    {
        return isset($value['dataDomain'])
            ? $value['dataDomain']
            : 0;
    }

    private static function generateDataSource(array $value = array())
    {
        return isset($value['dataSource'])
            ? $value['dataSource']
            : 0;
    }

    private static function generateRule(array $value = array())
    {
        return isset($value['rule'])
            ? $value['rule']
            : 0;
    }

    private static function generateEnumOptions(array $value = array())
    {
        return isset($value['enumOptions'])
            ? $value['enumOptions']
            : array();
    }

    private static function generateTemplate(array $value = array())
    {
        return isset($value['template'])
            ? $value['template']
            : array();
    }

    public static function generateResourceCatalogObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ResourceCatalog {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $dictionary = new ResourceCatalog($id);

        //name
        $name = self::generateName($faker, $value);
        $dictionary->setName($name);
        //applicationObject
        $applicationObject = self::generateApplicationObject($value);
        $dictionary->setApplicationObject($applicationObject);
        //dataDomain
        $dataDomain = self::generateDataDomain($value);
        $dictionary->setDataDomain($dataDomain);
        //dataSource
        $dataSource = self::generateDataSource($value);
        $dictionary->setDataSource($dataSource);
        //rule
        $rule = self::generateRule($value);
        $dictionary->setRule($rule);
        //enumOptions
        $enumOptions = self::generateEnumOptions($value);
        $dictionary->setEnumOptions($enumOptions);
        //template
        $template = self::generateTemplate($value);
        $dictionary->setTemplate($template);


        //createTime
        $dictionaryCreateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $dictionary->setCreateTime($dictionaryCreateTime);
        //updateTime
        $dictionaryUpdateTime = \Qxy\CreditData\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $dictionary->setUpdateTime($dictionaryUpdateTime);
        //statusTime
        $dictionaryStatusTime = \Qxy\CreditData\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $dictionary->setStatusTime($dictionaryStatusTime);
        //status
        $dictionaryStatus = \Qxy\CreditData\Common\Utils\MockFactory::generateStatus($faker, $value);
        $dictionary->setStatus($dictionaryStatus);

        return $dictionary;
    }
}
